#ifndef STACK_CLASS_HPP
#define STACK_CLASS_HPP

#include "ArrList.hpp"
#include <stdexcept>

template <typename T>
class Stack {
public:
    Stack();
    ~Stack();
    bool   empty() noexcept;
    size_t size() noexcept;
    T&     top();  // throw underflow
    void   push(T item) noexcept;
    void   pop();  // throw underflow
private:
    ArrList<T>* _list;
};


// Stack Functions
template <typename T>
Stack<T>::Stack()
  : _list(new ArrList<T>())
{}

template <typename T>
Stack<T>::~Stack()
{
    delete _list;
}

template <typename T>
bool Stack<T>::empty() noexcept
{
    return _list->size() == 0;
}

template <typename T>
size_t Stack<T>::size() noexcept
{
    return _list->size();
}

template <typename T>
T& Stack<T>::top()
{
    try {
        return _list->back();
    } catch (std::underflow_error& e) {
        throw e;
    }
}

template <typename T>
void Stack<T>::push(T item) noexcept
{
    _list->push_back(item);
}

template <typename T>
void Stack<T>::pop()
{
    try {
        _list->pop_back();
    } catch (std::underflow_error& e) {
        throw e;
    }
}

#endif