#include "ArrList.hpp"
#include <stack>
#include <iostream>

int main()
{
    std::stack<int, ArrList<int>> list;

    list.push(42);
    list.push(37);
    list.push(55);

    while (!list.empty()) {
        std::cout << list.top() << "  ";
        list.pop();
    }
    std::cout << '\n';

    return 0;
}