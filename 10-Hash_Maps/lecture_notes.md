# Hash Tables #

Some vocabulary to get out of the way

Dynamic set: a set that can grow, shrink, or change over time as it is manipulated by algorithms (data structure)
Dictionary: a dynamic set that supports the operations of insertion, deletion, and test membership

There are lots of situations where the dynamic set we want to use only needs these three operations. A real world example is a compiler. Element keys are character strings that correspond to identifiers in the language (it's how the compiler knows what new means, for example)

Hash tables generalize the notion of an array. Through the key value, we are able to directly address the array in a manner similar to arr[x]. Instead of using the key as an index value, the index value is calculated from the key.

## Direct-Address Tables ##

Direct addressing is great when the univese of keys is relatively small. We can use an array, we'll also call it a direct address table, to represent the dynamic set. Every element (or slot) of the direct-address table represents one and only one of the keys in the universe of all possible keys.

If a key is not being used, that slot is NIL. Otherwise it points to your data stored at that slot.

The search, insert, and delete functions are trivial at this point.

~~~~cpp
// Taken from the book
// T is a direct-address table, and k is a key value
search(T, k)
    return T[k]

insert(T, k)
    T[x.key] = x

delete(T, k)
    T[k] = NIL
~~~~

It should be observable that these operations all run in O(1) time. These are worst case times.

## Hash Tables ##

Direct-address tables are great under a very specific set of circumstances. If U is large, a table may be impractical. If the size of K (used keys) is small with relation to U, we waste memory.

If K is small compared to U, a hash table provides a good solution. It allows us to reduce our storage needs to K instead of U. We can also still search in O(1) time, although now this is an average case, and not worst case.

With direct addressing, a value was stored with a key k, and that slot was located at slot k. With a hash table, the slot is now located at h(k), where h() is a hash function.  The hash function reduces the key k to a new value that is usually much smaller, allowing our address table to be much smaller in memory.

### But If Hashed Values Are Smaller ###

It is possible for two keys to hash to the same value. This is called a collision. This can be very common, especially if we try to write our own hash functions. There are ways to work around collisions. It is best to try and avoid them altogether.

### Chaining ###

Chaining is a method of collision resolution. It doesn't avoid collisions, but when one occurs, chaining can help make sure that things still work. Basically, when a collision occurs, that slot is capable of containing a list of items. This is not ideal, but it can help resolve collisions with less effort.
