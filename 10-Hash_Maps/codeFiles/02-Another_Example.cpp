#include <algorithm>  // shuffle()
#include <chrono>     // system_clock
#include <iostream>
#include <random>  // default_random_engine
#include <unordered_map>
#include <vector>

void populateDepartments(std::unordered_map<std::string, std::string>& dep);
void populateEmployees(std::vector<std::string>& emp);
void mix(std::vector<std::string>& list);

int main()
{
    std::unordered_map<std::string, std::string> departments;
    std::vector<std::string>                     employees;

    populateDepartments(departments);
    populateEmployees(employees);
    mix(employees);

    for (auto& i : departments) {
        departments.at(i.first) = employees.back();
        employees.pop_back();
    }

    for (auto i : departments)
        std::cout << i.first << ": " << i.second << '\n';


    return 0;
}

void populateDepartments(std::unordered_map<std::string, std::string>& dep)
{
    dep.insert({ "Produce", std::string() });
    dep.insert({ "Makeup", std::string() });
    dep.insert({ "Deli", std::string() });
    dep.insert({ "Jewelry", std::string() });
    dep.insert({ "Baby", std::string() });
    dep.insert({ "Toys", std::string() });
    dep.insert({ "Clothing", std::string() });
    dep.insert({ "Electronics", std::string() });
}

void populateEmployees(std::vector<std::string>& list)
{
    list.push_back("Amy");
    list.push_back("Cheyenne");
    list.push_back("Dina");
    list.push_back("Garrett");
    list.push_back("Jonah");
    list.push_back("Justine");
    list.push_back("Kelly");
    list.push_back("Mateo");
    list.push_back("Sandra");
    list.push_back("Sayid");
}

void mix(std::vector<std::string>& list)
{
    size_t seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle(list.begin(), list.end(), std::default_random_engine(seed));
}
