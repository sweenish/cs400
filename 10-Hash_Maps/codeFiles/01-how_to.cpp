#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <string>
#include <unordered_map>  // hash tables
#include <utility>

/*
 * This file simply shows how to use some of the features of the unordered_map
 * class, along with a bit of a loop playground
 */
int main()
{
    std::unordered_map<std::string, std::string> assignments;

    assignments.insert({ "Produce", "Greg" });
    assignments.insert(std::make_pair<std::string, std::string>("Deli", "Penny"));
    assignments["Go Backs"] = "Jerry";  // operator[] can add a new item into the map
    try {
        assignments.at("Returns") =
          "Gina";  // at() will only return an item if the key exists already
    } catch (std::out_of_range& e) {
        std::cerr << "Can't create elements with at()\n";
    }

    /*
     * Iterators use a pair object so that they can be used to see the key and mapped value
     * The pair class has two public member variables, first & second
     *
     * The key is kept as a const key_type (we cannot change the keys once set with an iterator)
     */
    std::cout << "\n### Fun with for loops ###\n\"Traditional\"\n";
    for (std::unordered_map<std::string, std::string>::iterator it = assignments.begin();
         it != assignments.end(); ++it) {
        std::cout << it->first << ": " << (*it).second << '\n';
    }
    std::cout << '\n';

    // If you're iterating through the whole map, range-based for is a big boost to readability
    std::cout << "Range-based\n";
    for (auto i : assignments) {
        std::cout << i.first << ": " << i.second << '\n';
    }
    std::cout << '\n';

    // If you want to experiment with getting code really compact, <algorithm> has a lot of tools
    std::cout << "<algorithm> for_each()\n";
    std::for_each(assignments.begin(), assignments.end(),
                  [](auto i) { std::cout << i.first << ": " << i.second << '\n'; });
    std::cout << '\n';

    // A little up-front work can compact even that last loop
    auto print_roles_and_assignments = [](auto i) {
        std::cout << i.first << ": " << i.second << '\n';
    };
    auto for_each = [](auto container, auto func) {
        std::for_each(container.begin(), container.end(), func);
    };

    std::cout << "Overloading for_each\n";
    for_each(assignments, print_roles_and_assignments);

    return 0;
}