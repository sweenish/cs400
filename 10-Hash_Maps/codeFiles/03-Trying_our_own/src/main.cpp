#include "Hash_map.hpp"
#include <array>
#include <bitset>
#include <iomanip>
#include <iostream>
#include <optional>
#include <string>
#include <utility>
#include <vector>

template <typename T, unsigned long N>
void hash_and_add(std::array<std::pair<std::string, T>, N> arr, Hash_Map<T>& map);

template <typename T>
void print_map(Hash_Map<T>& map);

int main()
{
    Hash_Map<int> h;

    std::array<std::pair<std::string, int>, 5> indices = { std::make_pair("Answer", 42),
                                                           { "Why", 33 },
                                                           { "Hello", 55 },
                                                           { "Evil", 100 },
                                                           { "Vile", 89 } };

    hash_and_add(indices, h);

    // Creating a new Hash_Map object with a different hashing algorithm
    auto js = [](const unsigned char* str, unsigned int length) -> unsigned long {
        unsigned long hash = 1315423911;

        for (unsigned long i = 0; i < length; ++str, ++i) {
            hash ^= ((hash << 5) + (*str) + (hash >> 2));
        }

        return hash;
    };

    std::cout << '\n';

    Hash_Map<int> other(js);
    hash_and_add(indices, other);

    // Creating another Hash_Map with a poor hashing algorithm to guarantee a collision
    auto knuth = [](const unsigned char* str, unsigned int length) -> unsigned long {
        unsigned long hash = 0;

        for (unsigned long i = 0; i < length; ++str, ++i) {
            hash += *str;
        }

        return hash;
    };

    std::cout << '\n';

    Hash_Map<int> poor(knuth);
    hash_and_add(indices, poor);

    std::cout << "\nPrinting maps\nh\n";
    print_map(h);
    std::cout << "\nother\n";
    print_map(other);
    std::cout << "\npoor\n";
    print_map(poor);
}

template <typename T, unsigned long N>
void hash_and_add(std::array<std::pair<std::string, T>, N> arr, Hash_Map<T>& map)
{
    for (const auto i : arr) {
        unsigned char* tmp = new unsigned char[i.first.length() + 1];
        strncpy((char*)tmp, i.first.c_str(), i.first.length());
        unsigned long   hashValue = map.hash_function()(tmp, i.first.length());
        std::bitset<64> binary(hashValue);
        std::cout << "Hashing " << std::setw(8) << i.first << " results in: " << std::setw(20)
                  << hashValue << ": " << binary << '\n';
        map[i.first] = i.second;
    }
}

template <typename T>
void print_map(Hash_Map<T>& map)
{
    for (auto i : map) {
        std::cout << i.first << ": " << i.second << '\n';
    }
}