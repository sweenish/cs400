#ifndef HASH_MAP
#define HASH_MAP

// #define DEBUG=1  // Uncomment to see extra information about the hashing

#ifdef DEBUG
#include <bitset>
#endif

#include <cstdint>
#include <cstring>
#include <exception>
#include <optional>
#include <string>
#include <utility>
#include <vector>

// djb2 hashing algorithm, https://www.partow.net/programming/hashfunctions/#top
auto djb2 = [](const unsigned char* str, unsigned int length) -> unsigned long {
    unsigned long hash = 5381;
    unsigned long i    = 0;

    for (i = 0; i < length; ++str, ++i) {
        hash = ((hash << 5) + hash) + *str;
    }

    return hash;
};

// For simplicity's sake, the Key will always be a std::string, and collisions
// will overwrite existing data. The hashing function must also have the same
// signature.
template <typename T>
class Hash_Map {
public:
    using Hash = unsigned long (*)(const unsigned char* str, unsigned int length);

    Hash_Map();
    Hash_Map(Hash hashFunc);
    T& operator[](const std::string& key);
    T& at(std::string key);

    class iterator;
    iterator begin();
    iterator end();

    Hash hash_function() const;
    int  size() const;
    int  capacity() const;

private:
    // Data
    std::vector<std::optional<std::pair<std::string, T>>> map_m;

    Hash   hasher = djb2;
    size_t size_m = 0;

    // Helpers
    void grow();

    template <typename Container>
    uint64_t index(const Container& container, std::string key);
};


template <typename T>
class Hash_Map<T>::iterator {
public:
    iterator(std::optional<std::pair<std::string, T>>* start,
             std::optional<std::pair<std::string, T>>* finish);
    std::pair<std::string, T>& operator*();
    iterator&                  operator++();
    bool                       operator==(const iterator& rhs) const;
    bool                       operator!=(const iterator& rhs) const;

private:
    std::optional<std::pair<std::string, T>>* item_m;
    std::optional<std::pair<std::string, T>>* endpoint_m;
};


// Hash_Map implementation
template <typename T>
Hash_Map<T>::Hash_Map()
  : map_m(32)
{}

template <typename T>
Hash_Map<T>::Hash_Map(Hash hashFunc)
  : map_m(32)
  , hasher(hashFunc)
{}

template <typename T>
T& Hash_Map<T>::operator[](const std::string& key)
{
    uint64_t i = index(map_m, key);

    if (!map_m[i]) {
        if (static_cast<double>(size_m + 1) / map_m.capacity() > 0.7) {
            grow();
        }
        map_m[i] = std::make_pair(key, int());
        ++size_m;
    }

    if (map_m[i]->first != key) {
        map_m[i]->first = key;
    }

    return map_m[i]->second;
}

template <typename T>
T& Hash_Map<T>::at(std::string key)
{
    size_t i = index(map_m, key);

    if (!map_m[i]) {
        throw std::out_of_range("Key \"" + key + "\" does not exist\n");
    }

    return map_m[i]->second;
}

template <typename T>
typename Hash_Map<T>::iterator Hash_Map<T>::begin()
{
    return iterator(map_m.data(), map_m.data() + map_m.capacity());
}

template <typename T>
typename Hash_Map<T>::iterator Hash_Map<T>::end()
{
    auto addr = map_m.data() + map_m.capacity();
    return iterator(addr, addr);
}

template <typename T>
typename Hash_Map<T>::Hash Hash_Map<T>::hash_function() const
{
    return hasher;
}

template <typename T>
int Hash_Map<T>::size() const
{
    return size_m;
}

template <typename T>
int Hash_Map<T>::capacity() const
{
    return map_m.capacity();
}


template <typename T>
void Hash_Map<T>::grow()
{
    std::vector<std::optional<std::pair<std::string, T>>> tmp(map_m.capacity() * 2);
    for (auto i : map_m) {
        if (i) {
            tmp[index(tmp, i->first)] = std::move(map_m[index(map_m, i->first)]);
        }
    }

    map_m.swap(tmp);
}

template <typename T>
template <typename Container>
uint64_t Hash_Map<T>::index(const Container& container, std::string key)
{
    unsigned char* tmp = new unsigned char[key.length() + 1];
    strncpy((char*)tmp, key.c_str(), key.length());
#ifdef DEBUG
    std::bitset<64> hashed(hasher(tmp, key.length()));
    std::bitset<64> capacity(container.capacity() - 1);
    std::cout << "Key: " << tmp << '\n'
              << std::setw(15) << "Hash: " << hashed << '\n'
              << std::setw(15) << "Anding with: " << capacity
              << "\nReturning: " << (hasher(tmp, key.length()) & (container.capacity() - 1))
              << "\n\n";
#endif
    return hasher(tmp, key.length()) & (container.capacity() - 1);
}

// Hash_Map::iterator implementation
template <typename T>
Hash_Map<T>::iterator::iterator(std::optional<std::pair<std::string, T>>* start,
                                std::optional<std::pair<std::string, T>>* finish)
  : item_m(start)
  , endpoint_m(finish)
{
    while (!(*item_m) && item_m != endpoint_m) {
        ++item_m;
    }
}

template <typename T>
std::pair<std::string, T>& Hash_Map<T>::iterator::operator*()
{
    return **item_m;
}

template <typename T>
typename Hash_Map<T>::iterator& Hash_Map<T>::iterator::operator++()
{
    ++item_m;
    while (!(*item_m) && item_m != endpoint_m) {
        ++item_m;
    }
    return *this;
}

template <typename T>
bool Hash_Map<T>::iterator::operator==(const typename Hash_Map<T>::iterator& rhs) const
{
    return item_m == rhs.item_m;
}

template <typename T>
bool Hash_Map<T>::iterator::operator!=(const typename Hash_Map<T>::iterator& rhs) const
{
    return !(*this == rhs);
}

#endif

/*
 * Hashing is a one-way transformation. The goal is to take an input, and
 * transform it into a new, fixed-length, unique value. Another goal is that it
 * is very difficult for two inputs to hash to the same value. Common hashes
 * that you may be familiar with include CRC32, md5, and SHA.
 *
 * Hashing can be accomplished in many different ways, see the main.cpp for a
 * couple more examples. However, it is generally agreed that an effective hash
 * will include bit-shifts and XORs. In a cryptographically secure hash, (not
 * covered in this lecture, see CS 767), there are other requirements that must
 * be met, like a single bit change in the input resulting in at least a 50%
 * change in the bits of the resulting hash.
 *
 * This hash table is also using a trick to turn the hash values into indices.
 * The capacity of the vector starts at a minimum of 32, and will double when it
 * reaches 70% capacity. The doubling ensures that the capacity is always a
 * power of 2. The hash value is bitwise AND'd with the capacity minus 1. This
 * means that the last 5 bits of the hash value will select the index. This is
 * also why the initial capacity was chosen to be 32, to provide enough bits for
 * unique indices. If the initial capacity was 1 or 2, note that all values
 * would have been assigned to the same index. It should also be observed that
 * if the hash table grows, all existing values must be re-hashed.
 */