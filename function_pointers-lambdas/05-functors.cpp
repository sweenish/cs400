#include <array>
#include <chrono>
#include <functional>  // New include
#include <iomanip>
#include <iostream>
#include <random>

template <typename Container>
void
selection_sort(Container& arr, std::function<bool(int, int)> comparison);

template <typename Container>
void
print_container(const Container& arr);

bool
descending(int a, int b);

bool
ascending(int a, int b);

int
main()
{
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine         engine(seed);
    std::uniform_int_distribution<int> dist(1, 50);

    std::array<int, 10> a;

    for (auto& i : a)
        i = dist(engine);

    std::cout << std::setw(15) << std::left << "Initial State: ";
    print_container(a);

    selection_sort(a, ascending);  // The magic

    std::cout << std::setw(15) << std::left << "Sorted State: ";
    print_container(a);

    selection_sort(a, descending);  // The magic

    std::cout << std::setw(15) << std::left << "Sorted State: ";
    print_container(a);

    return 0;
}

template <typename Container>
void
selection_sort(Container& arr, std::function<bool(int, int)> comparison)
{
    for (unsigned int j = 0; j < arr.size() - 1; j++) {
        unsigned int keyIndex = j;
        for (unsigned int i = j + 1; i < arr.size(); i++) {
            if (comparison(arr.at(i), arr.at(keyIndex))) {
                keyIndex = i;
            }
        }
        std::swap(arr.at(j), arr.at(keyIndex));
    }
}

template <typename Container>
void
print_container(const Container& arr)
{
    for (auto i : arr)
        std::cout << i << ' ';
    std::cout << '\n';
}

bool
descending(int a, int b)
{
    return a > b;
}

bool
ascending(int a, int b)
{
    return a < b;
}

/*
 * The library <functional> (introduced with C++11) allows us use function pointers with some nicer
 * syntax. Again, we'll examine the prototype of selection_sort()
 *
 * template <typename Container>
 * void
 * selection_sort(Container& arr, std::function<bool(int, int)> comparison);
 *
 * Our second parameter has changed. std::function objects are wrappers around anything that is
 * callable. In this program it is holding a function pointer, but it can also hold the structs from
 * the previous example. Many times, especially in stricter object-oriented languages, everything
 * must be an object, even a simple function.
 *
 * We can see that std::function is a template class. It takes the same items as a function pointer.
 * We provide the return type and parameter list as the template parameter. We then name the functor
 * comparison. We are unable to specify template paramters, so we actually lose some flexibility in
 * this example.
 *
 * So, this works, and we could be done. But the final thing we could consider is the fact that
 * simple functions like ascending() and descending() are far too simple to really bother
 * implementing "officially," and they're also just one-off functions where portability really
 * doesn't matter.
 *
 * It is also worth noting that using std::function is very "expensive" compared to all other
 * methods. Best case, std::function generates 2x the lines of assembly. If performance is a
 * concern, there are much more efficient methods. The next method is more efficient without
 * sacrificing readability.
 */