#include <array>
#include <functional>
#include <iostream>
#include <set>
#include <string>
#include <utility>

// For this class, I am not going to define comparison operator overloads, because the method by
// which I choose to sort is situational.
class Pair {
public:
    Pair();
    Pair(int a, std::string b) : a(a), b(b) {}
    int                  getInt() const { return a; }
    std::string          getString() const { return b; }
    friend std::ostream& operator<<(std::ostream& sout, const Pair& rhs)
    {
        sout << "(" << rhs.a << ", " << rhs.b << ")";
        return sout;
    }

private:
    int         a = 0;
    std::string b = std::string();
};

// Templates are the ideal way to pass lambdas around*
// The Function type must meet the definition of Compare, defined here:
// https://en.cppreference.com/w/cpp/named_req/Compare
template <typename T, typename Function = std::less<void>>
decltype(auto)
make_set(Function&& sort)
{
    return std::set<T, Function>(std::forward<Function>(sort));
}

int
main()
{
    Pair a(42, "how");
    Pair b(3, "now");
    Pair c(23, "brown");
    Pair d(12, "cow");

    std::array<Pair, 4> tmp = {a, b, c, d};

    // In order to use these lambdas with my sets, they must be declared ahead of time
    // Lambdas should be assigned to auto
    auto intDescending = [](const Pair& lhs, const Pair& rhs) -> bool {
        return lhs.getInt() < rhs.getInt();
    };
    auto strDescending = [](const Pair& lhs, const Pair& rhs) {
        return lhs.getString() < rhs.getString();
    };

    // I will create two sets, one that sorts the pairs by integer, and another that sorts by
    // strings Both will sort from low to high
    // The lambdas for how to sort are being passed to the function make_set
    auto byInt = make_set<Pair>(intDescending);
    auto byStr = make_set<Pair>(strDescending);

    for (int i = 0; i < 4; ++i) {
        byInt.insert(tmp.at(i));
        byStr.insert(tmp.at(i));
    }

    std::cout << "Printing byInt:\n";
    for (auto i : byInt)
        std::cout << i << ' ';
    std::cout << "\n\n";

    std::cout << "Printing byStr:\n";
    for (auto i : byStr)
        std::cout << i << ' ';
    std::cout << '\n';
}

/*
 * There's actually a decent bit of code here that likely hasn't been seen before. I will provide a
 * short overview.
 *
 * std::set<> is a container class that ensures every item is unique, and that the set is always
 * sorted. If possible, the std::set will use overloaded comparison operators, but as above, we may
 * need to define how to sort a class manually.
 *
 * The function make_set has some crazy stuff going on. std::forward should look familiar, but the
 * return type is likely new. C++11 allows you to return auto, with the added requirement of a
 * trailing return type. We don't have a trailing return type because C++14 made auto return more
 * robust. An example of a trailing return type can be seen in the lambda declrations, though.
 * decltype() is another way to deduce types with slightly different rules than auto. The biggest
 * difference is that auto will strip cv-qualifiers (const, volatile) and reference-ness while
 * decltype will not. decltype rules are generally preferred for function return type deduction.
 * What decltype(auto) does is tell the compiler to deduce the return type using declytpe rules.
 *
 * make_set() is also a template function, and the sorting function is a template parameter.
 * Providing a sort function is optional, and if not provided, will default to trying to use the
 * operator<() function. One important note on this function. Pre-C++11 the template parameter line
 * would be required to look like this (note the space near the end):
 * template<typename T, typename Function = std::less<void> >
 *
 * The lambdas are assigned to auto objects. This is the best way to assign lambdas. The reason is
 * because the lambda expression causes the compiler to generate a closure class. The type of this
 * class is unknown to us, but it is known to the compiler. So, with auto the compiler will ensure
 * that the correct type is used for the closure. std::function is "heavy," and may allocate on the
 * heap, which may not be desireable.
 *
 * TODO: need something with captures
 */