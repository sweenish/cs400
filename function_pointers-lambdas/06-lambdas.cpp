#include <array>
#include <chrono>
#include <functional>
#include <iomanip>
#include <iostream>
#include <random>

template <typename Container, typename Function>
void
selection_sort(Container& arr, Function comparison);

template <typename Container>
void
print_container(const Container& arr);

int
main()
{
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine         engine(seed);
    std::uniform_int_distribution<int> dist(1, 50);

    std::array<int, 10> a;

    for (auto& i : a)
        i = dist(engine);

    std::cout << std::setw(15) << std::left << "Initial State: ";
    print_container(a);

    selection_sort(a, [](int a, int b) { return a < b; });  // The magic

    std::cout << std::setw(15) << std::left << "Sorted State: ";
    print_container(a);

    auto descending = [](int a, int b) { return a > b; };
    // std::function<bool(int, int)> descending = [](int a, int b) { return a > b; };
    // ^^ similar, but inferior declaration
    selection_sort(a, descending);  // The magic

    std::cout << std::setw(15) << std::left << "Sorted State: ";
    print_container(a);

    return 0;
}

template <typename Container, typename Function>
void
selection_sort(Container& arr, Function comparison)
{
    for (unsigned int j = 0; j < arr.size() - 1; j++) {
        unsigned int keyIndex = j;
        for (unsigned int i = j + 1; i < arr.size(); i++) {
            if (comparison(arr.at(i), arr.at(keyIndex))) {
                keyIndex = i;
            }
        }
        std::swap(arr.at(j), arr.at(keyIndex));
    }
}

template <typename Container>
void
print_container(const Container& arr)
{
    for (auto i : arr)
        std::cout << i << ' ';
    std::cout << '\n';
}

// TODO Discuss why auto declaration is preferred
/*
 * Before we discuss this code, we need to get some vocabulary sorted.
 * - Lambda expression: An expression. In line 28, [](int a, int b) { return a < b; } is the lambda
 * expression.
 * - Closure: the runtime object created by the lambda. In line 28, the object passed to
 * selection_sort() as the second argument is the closure
 * - Closure class: the class from which the closure is instantiated. Lambdas cause compilers to
 * create unique closure classes. To see this in action, copy/paste this code at cppinsights.io and
 * hit play. The right pane exposes what the compiler is doing for us behind the scenes.
 *
 * Typically, these terms end up getting mixed together, and people will generally know what you're
 * talking about. But a better vocabulary helps you communicate more effectively and broadens your
 * own understanding.
 *
 * Lambdas are an interesting topic for a few reasons. The most surprising is that they technically
 * bring nothing completely new to the language. We've been able to pass functions around and create
 * functors before C++11, but it's the conveniences of lambdas that make them one of the biggest
 * features of C++11.
 *
 * We can also observe that I have removed the functions ascending() and descending(), since they're
 * no longer needed. Instead, the new syntax is now placed in on lines 25 and 30, shown below:
 *
 * selection_sort(a, [](int a, int b){return a < b;});
 * std::function<bool(int, int)> descending = [](int a, int b){return a > b;};
 *
 * In program 4, we passed already existing functions to selection_sort(). On line 25, we are
 * declaring, implementing, and passing our function all in one go. In line 30, we declare a functor
 * and initialize it with a lambda. We then pass the functor along to selection_sort().
 *
 * Now, let's examine the syntax of a lambda. Basic lambda syntax looks like this:
 * [_captures_](_parameters_){_body_}
 * We'll look at each section from right to left.
 *
 * Body:
 * This is also something easy. It's the body of the function. Nothing really changes here.
 * Lambdas have a tendency to be very small functions, and that's why so far they are all typed out
 * on one line. But lambdas can be as long as they need to be, contain as many statements as they
 * need, and can occupy as many lines as necessary. For the function body, we use the syntax that we
 * are used to.
 *
 * Parameters:
 * This is simply a parameter list, same as we've seen for any other function.
 *
 * Captures:
 * The idea here is that functors have scope. Sometimes when we want a lambda, it is to interact
 * with things outside the scope of the functor. Instead of bloating parameter lists, we allow the
 * capture area to specify how out-of-scope objects are brought into the lambda.
 *
 * Leaving the [] empty as seen above simply means I am not capturing anything.
 *
 * There are two default modes for capturing out-of-scope objects: copy and by reference. We can
 * indicate copy capture like so: [=] Copy capture comes with an extra 'restriction.' The copies are
 * const by default. If we want to copy capture, but also need to be able to modify the copy, we
 * must add the specifier mutable after the parameter list. It would look something like this
 * [=](_parameters_) mutable {_body_};
 *
 * I can also default to capturing by reference, with [&].
 *
 * Defaults have the potential to be dangerous, since they are a blank check to bring in all
 * out-of-scope objects as copies or by reference.
 *
 * The capture block can contain a comma separated list of items. You cannot place both capture
 * defaults in a single capture block. But you can place one default, and if you need to capture one
 * (or more) object(s) differently, in the capture section like so: [=, &i]
 *
 * Lambdas make interacting with many STL functions so much easier.
 *
 * Lambdas were introduced in C++11, but are still evolving. You could not provide default values to
 * lambda parameters before C++14. In C++20, it is proposed to allow lambdas to take template
 * typename parameters. Two more specifiers have been added: constexpr (C++14) and consteval
 * (C++17).
 *
 * Finally, the selection sort function header is changed one last time. Using templates is more
 * efficient than function pointers, while being easier to read than std::function. There is one
 * caveat, and that's the fact that the template will accept anything you give it. This generally
 * means that it is the responsibility of the engineer to document the requiremtns for the function
 * parameter. Failure of users of your API to comply will usually result in a compile error if they
 * pass the wrong type or an incorrect function.
 *
 * The next program will demonstrate a recursive lambda.
 */