#include <array>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>

template <typename Container>
void
selection_sort(Container& arr);

template <typename Container>
void
print_container(const Container& arr);

template <typename T>
bool
descending(const T& a, const T& b);

template <typename T>
bool
ascending(const T& a, const T& b);

int
main()
{
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine         engine(seed);
    std::uniform_int_distribution<int> dist(1, 50);

    std::array<int, 10> a;

    for (auto& i : a)
        i = dist(engine);

    std::cout << std::setw(15) << std::left << "Initial State: ";
    print_container(a);

    selection_sort(a);  // The magic

    std::cout << std::setw(15) << std::left << "Sorted State: ";
    print_container(a);

    return 0;
}

template <typename Container>
void
selection_sort(Container& arr)
{
    for (unsigned int j = 0; j < arr.size() - 1; j++) {
        unsigned int keyIndex = j;
        for (unsigned int i = j + 1; i < arr.size(); i++) {
            if (descending(arr.at(i), arr.at(keyIndex))) {
                keyIndex = i;
            }
        }
        std::swap(arr.at(j), arr.at(keyIndex));
    }
}

template <typename Container>
void
print_container(const Container& arr)
{
    for (auto i : arr)
        std::cout << i << ' ';
    std::cout << '\n';
}

template <typename T>
bool
descending(const T& a, const T& b)
{
    return a > b;
}

template <typename T>
bool
ascending(const T& a, const T& b)
{
    return a < b;
}

/*
 * Here, we have removed the comparison from selection sort to its own function. The original
 * comparison can still be achieved by swapping out the call in selection_sort() from descending to
 * ascending. This gives us both, but it still doesn't feel very good.
 *
 * a) I have to alter the code of selection_sort() to change how it sorts
 * b) I feel obligated to provide some basic comparisons that users can choose
 * c) selection_sort() can only ever sort one way for the duration of a program; this may not be
 * desirable
 * d) If users want a more specialized sort, they will not only have to write that comparison
 * function, but also alter selection_sort()
 */