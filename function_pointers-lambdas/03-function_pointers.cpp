#include <array>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>

template <typename Container, typename T>
void
selection_sort(Container& arr, bool (*comparison)(T, T));

template <typename Container>
void
print_container(const Container& arr);

template <typename T>
bool
descending(const T& a, const T& b);

template <typename T>
bool
ascending(const T& a, const T& b);

int
main()
{
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine         engine(seed);
    std::uniform_int_distribution<int> dist(1, 50);

    std::array<int, 10> a;

    for (auto& i : a)
        i = dist(engine);

    std::cout << std::setw(15) << std::left << "Initial State: ";
    print_container(a);

    selection_sort(a, ascending<decltype(a.at(0))>);  // The magic

    std::cout << std::setw(15) << std::left << "Sorted State: ";
    print_container(a);

    selection_sort(a, descending<decltype(a.at(0))>);  // The magic

    std::cout << std::setw(15) << std::left << "Sorted State: ";
    print_container(a);

    return 0;
}

template <typename Container, typename T>
void
selection_sort(Container& arr, bool (*comparison)(T, T))
{
    for (unsigned int j = 0; j < arr.size() - 1; j++) {
        unsigned int keyIndex = j;
        for (unsigned int i = j + 1; i < arr.size(); i++) {
            if (comparison(arr.at(i), arr.at(keyIndex))) {
                keyIndex = i;
            }
        }
        std::swap(arr.at(j), arr.at(keyIndex));
    }
}

template <typename Container>
void
print_container(const Container& arr)
{
    for (auto i : arr)
        std::cout << i << ' ';
    std::cout << '\n';
}

template <typename T>
bool
descending(const T& a, const T& b)
{
    return a > b;
}

template <typename T>
bool
ascending(const T& a, const T& b)
{
    return a < b;
}

/*
 * We're going to start by looking at the new prototype for selection_sort():
 *
 * template <typename Container, typename T>
 * void
 * selection_sort(Container& arr, bool (*comparison)(T, T));
 *
 * That nasty looking second parameter is a function pointer. Remember that when programs are
 * compiled, the program is built into a stack. Any given function resides at one specific location
 * in the stack. When a function call is made, this is usually represented as the address where the
 * function lives in the stack. The flow moves to the location of the function, and when the
 * function finishes, flow returns to just after the function redirect.
 *
 * This means that functions do have addresses, and we can use that knowledge to our advantage. The
 * function pointer is not so bad when we look at it closer. It shows a return type (bool), we name
 * the pointer (comparison), and we provide a parameter list (int, int). The reason that
 * (*comparison) *must* be surrounded by () is because otherwise it will look like a function
 * prototype that returns a pointer to a bool. So, comparison is a pointer to a function that
 * returns a bool and takes two integers as parameters.
 *
 * Note in the selection_sort() function, I can use the function pointer just like an actual
 * function. This is because the compiler has always been implicitly de-referencing the functin
 * pointer for us.
 *
 *
 *
 * Like normal pointers, I can assign to a function pointer as well. When doing so, everything has
 * to match (return type, and parameter list). When assigning a function to a function pointer, do
 * *NOT* use ().
 *
 * void (*fPtr)(int) = foo;
 *
 * As long as foo() is a void function that accepts a single integer, it will be assigned to a
 * function pointer called fPtr as seen above. Trying to assign foo() is wrong, as that calls the
 * function (incorrectly), and doesn't provide the pointer.
 *
 * We can see the increased functionality that this provides. I call selection_sort() twice in
 * main(), and I am able to sort in both ascending and descending order in the same program. I no
 * longer have to modify selection_sort() to change how I make my comparison.
 *
 * Note the calls to selection sort in main(). Because the functions descending() and ascending()
 * are templates, they need to know what type to overload. If I pass just the function name, the
 * compiler is unable to determine the type for the sorting function. So I am telling the compiler
 * what overload to use based on the first element of my container. decltype() is a function that
 * returns the type of its parameter
 */