#include <functional>
#include <iostream>
#include <iterator>
#include <vector>

int
main()
{
    int              val;
    std::vector<int> fib;

    std::cout << "Sequence length? ";
    std::cin >> val;

    std::function<int(int)> seq = [&seq](int val) {
        return val <= 1 ? val : seq(val - 1) + seq(val - 2);
    };
    // std::function<int(int)> seq = [&](int val){ return val <= 1 ? val :
    // seq(val - 1) + seq(val - 2); };
    /*
     * The lambda above (commented out) is also valid, but it is less specific. It will capture all
     * out-of-scope objects by reference, but in line 15 I am guaranteeing that I only capture seq,
     * the functor holding my lambda.
     *
     * I would recommend avoiding general captures.
     *
     * The lambda must be assigned to a std::function object because it captures itself. This makes
     * it incompatible with an auto declaration, as it would create a deduction circle.
     *
     * It could also look like this:
     * std::function<int(int)> seq = [&seq](int val) {
     *      if (val <= 1)
     *          return val;
     *      else
     *          return seq(val - 1) + seq(val - 2);
     * }
     *
     * Lambdas can be as long as needed, and you would generally format them like any other
     * function.
     *
     * The implementation above works, but it's not ideal due to some limitations of std::function,
     * see this page for details: https://riptutorial.com/cplusplus/example/8508/recursive-lambdas
     */

    for (int i = 0; i < val; i++)
        fib.push_back(seq(i));

    for (auto i : fib)
        std::cout << i << ' ';
    std::cout << '\n';

    return 0;
}