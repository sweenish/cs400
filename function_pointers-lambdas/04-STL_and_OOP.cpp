/*
 * We're going to step away from our current example to illustrate another way that this behavior
 * was coded before C++11.
 */

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

template <typename T>
struct Descending {
    bool operator()(T lhs, T rhs) const { return lhs < rhs; }
};

template <typename T>
struct Ascending {
    bool operator()(T lhs, T rhs) const { return lhs > rhs; }
};

// I'm demonstrating pre-C++11 function passing, not iterating
template <typename Container>
void
print(const Container& v)
{
    for (auto i : v)
        std::cout << i << ' ';
    std::cout << '\n';
}

int
main()
{
    using std::begin;
    using std::cout;
    using std::end;
    using std::string;
    using std::vector;

    vector<int>    vInt = {25, 45, 12, 6, 16};
    vector<string> vStr = {"how", "now", "brown", "cow"};

    cout << "### Integers ###\n" << std::setw(12) << std::left << "Original: ";
    print(vInt);
    std::sort(begin(vInt), end(vInt), Descending<int>());
    cout << std::setw(12) << std::left << "Descending: ";
    print(vInt);
    std::sort(begin(vInt), end(vInt), Ascending<int>());
    cout << std::setw(12) << std::left << "Ascending: ";
    print(vInt);

    cout << "\n\n";
    cout << "### Strings ###\n" << std::setw(12) << std::left << "Original: ";
    print(vStr);
    std::sort(begin(vStr), end(vStr), Descending<string>());
    cout << std::setw(12) << std::left << "Descending: ";
    print(vStr);
    std::sort(begin(vStr), end(vStr), Ascending<string>());
    cout << std::setw(12) << std::left << "Ascending: ";
    print(vStr);
}

/*
 * TODO: Explain this
 * The <algorithm> function std::sort() existed before C++11. The specific invocation of the
 * function looks like this:
 *
 * template< class RandomIt, class Compare >
 * void sort( RandomIt first, RandomIt last, Compare comp );
 *
 * The first two parameters are iterators (typically just begin() and end()), and the third
 * parameter is how we are passing the function to sort. Compare is a template type, and it will
 * assume the type of the function passed to it. Now, in order to work, the parameter comp must be a
 * binary comparison function.
 *
 * The two structs declared at the top are typically called functors, or function objects. We have
 * taken our sorting functions, and wrapped them with a struct. They are executed by invoking the
 * class's
 * () operator (function call operator).
 *
 * A bonus to this method over function pointers is that I don't have to deal with function
 * pointers. It also is better OOP to have these functions contained.
 *
 * Some downsides to this method are that I cannot make these local classes, i.e., I cannot declare
 * them in the same scope I intend to use them. Another downside is that in passing the functor to
 * std::sort, it's ambiguous whether I am invoking a function call operator or a constructor. The
 * compiler gets it sorted out ( ¯\_(ツ)_/¯ ), but the code becomes that much more confusing to
 * read.
 */