#include <array>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>

template <typename Container>
void
selection_sort(Container& arr);

template <typename Container>
void
print_container(const Container& arr);

int
main()
{
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine         engine(seed);
    std::uniform_int_distribution<int> dist(1, 50);

    std::array<int, 10> a;

    for (unsigned int i = 0; i < a.size(); i++)
        a.at(i) = dist(engine);

    std::cout << std::setw(15) << std::left << "Initial State: ";
    print_container(a);

    selection_sort(a);

    std::cout << std::setw(15) << std::left << "Sorted State: ";
    print_container(a);

    return 0;
}

template <typename Container>
void
selection_sort(Container& arr)
{
    for (unsigned int j = 0; j < arr.size() - 1; j++) {
        unsigned int minIndex = j;
        for (unsigned int i = j + 1; i < arr.size(); i++) {
            if (arr.at(i) < arr.at(minIndex)) {
                minIndex = i;
            }
        }
        std::swap(arr.at(j), arr.at(minIndex));
    }
}

template <typename Container>
void
print_container(const Container& arr)
{
    for (auto i : arr)
        std::cout << i << ' ';
    std::cout << '\n';
}

/*
 * There shouldn't be anything shocking in this program. A container (std::array in this case) is
 * filled with random values, and the array is sorted in ascending order using a selection sort
 * algorithm.
 *
 * The question being posed now, is what if I want to employ selection sort, but in descending
 * order? The expectation of re-writing and renaming the algorithm to just change one sign doesn't
 * sit well.
 */