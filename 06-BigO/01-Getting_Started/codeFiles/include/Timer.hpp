#ifndef TIMER_HPP
#define TIMER_HPP

#include <chrono>

class Timer {
    std::chrono::high_resolution_clock::time_point m_begin;
    std::chrono::high_resolution_clock::time_point m_end;

public:
    Timer() = default;
    void      start();
    void      stop();
    long long milliseconds();
};

// Implementation
void Timer::start()
{
    m_begin = std::chrono::high_resolution_clock::now();
}

void Timer::stop()
{
    m_end = std::chrono::high_resolution_clock::now();
}

long long Timer::milliseconds()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(m_end - m_begin).count();
}
#endif