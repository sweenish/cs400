#ifndef SORT_SORT_HPP
#define SORT_SORT_HPP

#include <algorithm>
#include <iostream>
#include <limits>
#include <utility>
#include <vector>

// The sorts in this file:
//  - Insertion
//  - Merge
//  - Selection
//  - Counting
//  - Heap

/*
 * In order to discuss runtime efficiency, we need to define a couple terms.
 * Running Time: The number of "steps" executed. We are going to consider a
 * step be a line of code. While different lines of code may take different
 * amounts of time to execute, we will consider that each line takes some
 * constant amount of time
 *
 * Size of Input: Depends on the problem being examined. What's nice is that for
 * studying sorting algorithms, we can simply use the size of the data to be
 * sorted
 *
 * Now, we need to think about what situations we will typically consider.
 * This program runs based on "average" run-time. But when measuring run times,
 * it's usually better to look at the worst possible case. By doing it this
 * way, we know that our algorithm cannot run worse than that; we have found
 * an upper bound.
 *
 * The reason this program uses average run-time instead of worst case is
 * that the average case and worst-case are not that far off in the cases of
 * these sorts.
 *
 * We call the calculation of run-time efficiency in the worst case big-O.
 * We therefore use big-O notation to describe the running time in terms of the
 * size of input, n. Some examples:
 * O(1) - algorithm runs in constant time. This is the dream
 * O(n) - algorithm scales linearly with the size of the input
 * O(lg n) - algorithm scales logarithmically with the size of the input; this
 *           is better than linear scaling
 * O(n^k) - algorithm scales at a rate of n^k where k is a constant; still considered
 *          fast. Also called polynomial time
 * O(k^n) - exponential running time, not desirable at all. Sometimes unavoidable
 */


/*
 * Insertion sort is akin to how we sort a hand of cards. We use the first card
 * as a starting point, and beginning with the second card, place it accordingly
 * to our sorted section
 *
 * Insertion Sort runs in O(n^2) time
 */
auto insertionSort = [](std::vector<uint64_t>& a) {
    // std::cout << __func__ << '\n';
    int key;
    int i;

    // Pseudo-code from CLRS, p. 18 (NOTE: All arrays are 1-based in this book)
    // INSERTION-SORT(A)
    // for j = 2 to A.length
    //     key = A[j]
    //     // Insert A[j] into the sorted sequence A[1..j - 1]
    //     i = j - 1
    //     while i > 0 and A[i] > key
    //         A[i + 1] = A[i]
    //         i = i - 1
    //     A[i + 1] = key

    for (unsigned int j = 1; j < a.size(); j++) {
        key = a[j];
        i   = j - 1;
        while (i >= 0 && a[i] > key) {
            a[i + 1] = a[i];
            i--;
        }
        a[i + 1] = key;
    }
};


// This function is used as a part of merge sort
void merge_parts(std::vector<uint64_t>& a, int beg, int mid, int end)
{
    // Pseudo-code from CLRS, p. 31
    // MERGE(A, p, q, r)
    // n1 = q - p + 1
    // n2 = r - q
    // let L[1..n1 + 1] and R[1..n2 + 1] be new arrays
    // for i = 1 to n1
    //     L[1] = A[p + i - 1]
    // for j = 1 to n2
    //     R[j] = A[q + j]
    // L[n1 + 1] = inf
    // R[n2 + 1] = inf
    // i = 1
    // j = 1
    // for k = p to r
    //     if L[i] <= R[j]
    //         A[k] =  L[i]
    //         i++
    //     else
    //         A[k] = R[j]
    //         j++

    int                   lhsLength = mid - beg + 1;
    int                   rhsLength = end - mid;
    std::vector<uint64_t> lhs(lhsLength + 1);
    std::vector<uint64_t> rhs(rhsLength + 1);

    for (int i = 0; i < lhsLength; i++)
        lhs[i] = a[beg + i];

    for (int i = 0; i < rhsLength; i++)
        rhs[i] = a[mid + i + 1];

    lhs[lhsLength] = std::numeric_limits<int>::max();
    rhs[rhsLength] = std::numeric_limits<int>::max();

    int i = 0;
    int j = 0;
    for (int k = beg; k <= end; k++) {
        if (lhs[i] <= rhs[j]) {
            a[k] = lhs[i];
            i++;
        } else {
            a[k] = rhs[j];
            j++;
        }
    }
}


/*
 * Merge employs what is called a Divide and Conquer strategy. We can see in
 * the function below that it recursively splits the problem into halves until
 * the solutions are trivial, then the merge() funtion is called which joins
 * the two halves together.
 *
 * Merge sort runs in O(nlg n) time
 */
void mergeSort_main(std::vector<uint64_t>& a, int start, int end)
{
    // Pseudo-code form CLRS p. 34
    // MERGE-SORT(A, p, r)
    // if p < r
    //     q = ⌊(p + r) / 2⌋
    //     MERGE-SORT(A, p, q)
    //     MERGE-SORT(A, q + 1, r)
    //     MERGE(A, p, q, r)

    if (start < end) {
        int mid = (start + end) / 2;
        mergeSort_main(a, start, mid);
        mergeSort_main(a, mid + 1, end);
        merge_parts(a, start, mid, end);
    }
}

void mergeSort(std::vector<uint64_t>& a)
{
    // std::cout << __func__ << '\n';
    mergeSort_main(std::forward<decltype(a)>(a), 0, a.size() - 1);
}

/*
 * Selection sort works by selecting the smallest (or largest) element and
 * placing at the appropriate end of the sorted portion of the array.
 *
 * Selection sort runs in O(n^2) time
 */
void selectionSort(std::vector<uint64_t>& a)
{
    // std::cout << __func__ << '\n';
    // No pseudo-code for this one, but I've discussed it in class since
    // CS 211. This is a min-Selection sort, as it finds the smallest element
    // and places it just before the unsorted elements start.
    //
    // Some lines are saved by using std::swap() from <utility>

    int min;

    for (unsigned int k = 0; k < a.size() - 1; k++) {
        min = k;

        for (unsigned int i = k + 1; i < a.size(); i++) {
            if (a[i] < a[min]) {
                min = i;
            }
        }
        std::swap(a[k], a[min]);
    }
}


/*
 * Counting sort is different from the three previous sorts in a couple ways.
 * The first, and most easily observable, is that counting sort does not
 * require that any comparisons be made in order to sort the array.
 *
 * The second is that counting sort is not "in place" like the previous arrays.
 * The prior three sorts require no extra storage beyond the array itself to do
 * their jobs. Counting sort requires 2n + k memory space. The original array,
 * the sorted array, and a third array to count all possible values in the
 * array. Typically, counting sort makes sense when n (the size of the
 * array) and k (the maximum value held in the array) are close to each other.
 *
 * Counting sort runs in O(k + n) time. When k and n are close, we say that
 * counting sort runs in O(n) time.
 */
std::vector<uint64_t> countingSort_main(std::vector<uint64_t> a, int k)
{
    // Pseudo-code from CLRS p. 195
    // COUNTING-SORT(A, B, k)
    // let C[0..k] be a new array
    // for i = 0 to k
    //     C[i] = 0
    // for j = 1 to A.length
    //     C[A[j]] = C[A[j]] + 1
    // // C[i] now contains the number of elements equal to i; Note 1
    // for i = 1 to k
    //     C[i] = C[i] + C[i - 1]
    // // C[i] now contains the number of elements less than or equal to i; Note 2
    // for j = A.length downto 1
    //     B[C[A[j]]] = A[j]
    //     C[A[j]] = C[A[j]] - 1
    // // Note 3

    std::vector<std::uint64_t> counts(k + 1);
    std::vector<uint64_t>      b;
    for (unsigned int i = 0; i < a.size(); i++)
        b.push_back(-1);

    for (int i = 0; i < k + 1; i++)
        counts.push_back(0);

    for (int i = 0; i < static_cast<int>(a.size()); i++)  // Note 1
        counts[a[i]]++;

    for (int i = 1; i < k + 1; i++)  // Note 2
        counts[i] += counts[i - 1];

    for (int k = static_cast<int>(a.size() - 1); k >= 0; k--) {  // Note 3
        try {
            b[counts[a[k]] - 1] = a[k];
        } catch (std::out_of_range& e) {
            std::cout << "k: " << k << "\n"
                      << e.what() << "\n"
                      << "a[" << k << "]: " << a[k] << "\n"
                      << "counts[" << a[k] << "]: " << counts[a[k]] << "\n";
            for (auto i : b)
                std::cout << i << "  ";

            std::cout << "\n\n";
        }
        counts[a[k]]--;
    }

    return b;
}
/*
 * NOTE 1: The variable k that is passed to the counting sort is the maximum
 * value held in the array to be sorted. Meaning, if the array holds a highest
 * value of 100, k = 100. So, what this means is that when this loop ends, the
 * array counts holds the number of occurrences of a value in the array to be
 * sorted. If my original array holds values up to 100, the counts array will be
 * size 101. If the orignal array has 3 elements with a value of 54, then
 * counts[54] = 3 at the end of the loop.
 *
 * NOTE 2: The second for loop alters the counts array so that each element
 * now represents a location in the sorted array. For a trivial case where
 * a = [3, 1, 3], the initial values in counts would be [0, 1, 0, 2]. After this
 * for loop, counts = [0, 1, 1, 3]. counts[3] = 3 is saying that there are three
 * values before 3 in the sorted array.
 *
 * NOTE 3: The setup of Notes 1 & 2 allow this loop to automatically place
 * elements in the correct location in the sorted array. The decrement in this
 * loop allows for the possibility of repeated values in the array to be sorted.
 *
 * The diagrams in CLRS help illustrate this.
 */

void countingSort(std::vector<uint64_t>& a)
{
    // std::cout << __func__ << '\n';
    a = countingSort_main(a, *std::max_element(a.begin(), a.end()));
}

/*
 * We'll discuss heapsort here. Unlike the other sorts, this one is implmented
 * already in the <algorithm> library. The method of using it is a little
 * weird. But first, we should talk about heaps.
 *
 * A heap in the context of sorting, is a data structure. This is not to be
 * mixed up with the dynamic memory heap where we allocate things at runtime.
 *
 * A heap is very similar to a binary search tree, but it is less strict in its
 * requirements. One property that a heap requires, that a binary search does
 * not, is that a heap is completely filled on all levels, except for the
 * bottom. We will discuss a max-heap, where the following rule applies:
 * PARENT >= CHILD. Refer to Section 6.1 in the textbook.
 *
 * Because of the binary tree nature of a heap (and the requirement that all
 * levels must be full), we can also represent it in an array. Starting at the
 * top node (root), moving down and left to right, we number each node. These
 * numbers correspond to where in the array that node can be found. We can
 * navigate the array like a tree using the following properties:
 *
 * Given a "node" at index i in the array:
 * PARENT = floor(i / 2)    // Floor is essentially integer division, (always
 *                          // round down)
 * LEFT = 2i
 * RIGHT = 2i + 1
 *
 * The height of the heap is lg n, where n is the number of elements in the
 * tree. We can count the height by finding the longest simple downward path.
 * We start counting from the bottom-most node (calle a leaf) up to the root,
 * and we start counting at zero.
 *
 * Now we can talk about heapsort. The idea here is relatively straightforward.
 * We swap the root with the last element, shrink the heap by one, and
 * re-heapify.
 */
void heapSort(std::vector<uint64_t>& a)
{
    // std::cout << __func__ << '\n';
    std::make_heap(a.begin(), a.end());
    std::sort_heap(a.begin(), a.end());
}

#endif