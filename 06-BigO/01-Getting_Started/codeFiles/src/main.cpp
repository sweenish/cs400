/* This file will give one look at how multi-threading can be handled in C++ */

#include "Timer.hpp"
#include "sorts.hpp"
#include <cstdint>
#include <functional>
#include <future>
#include <iostream>
#include <limits>
#include <mutex>
#include <random>
#include <string>
#include <thread>
#include <vector>

/*** Global declarations ***/
static std::mutex m;

/*** Function Declarations ***/
void fill(std::vector<std::uint64_t>& a,
          std::uint64_t               size,
          std::uint64_t               maxValue,
          std::mt19937&               prng);

void sort_container(std::string                 sortAlgo,
                    std::vector<std::uint64_t>& v,
                    void (*sortFunc)(std::vector<std::uint64_t>&));

int main(int argc, char** argv)
{
    // Process CLI arguments
    // First ensure that a size was passed along
    if (argc != 2) {
        std::cerr << "USAGE: ./sorts SIZE\n";
        return 1;
    }

    // Ensure argument to program was indeed an integer
    std::string   length(argv[1]);
    std::size_t   loc;
    std::uint64_t givenSize = 0;
    try {
        givenSize = std::stoi(length, &loc);
    } catch (std::invalid_argument& e) {
        std::cerr << "Please enter a number.\n";
        return 2;
    } catch (std::out_of_range& e) {
        std::cerr << "Please enter a smaller number.\n";
        return 3;
    }
    if (loc != length.length()) {
        std::cerr << "Argument to program must be an integer.\n";
        return 4;
    }

    // Check size of integer
    std::uint64_t maxValue =
      std::numeric_limits<std::uint64_t>::max() / givenSize >= 10 ? givenSize * 10 : 0;
    if (maxValue == 0) {
        std::cerr << "Please enter a number 10% of the maximum value of a uint64_t.\n";
        return 5;
    }

    std::random_device rd;
    std::mt19937       prng(rd());

    std::vector<std::uint64_t> one;
    std::vector<std::uint64_t> two;
    std::vector<std::uint64_t> three;
    std::vector<std::uint64_t> four;
    std::vector<std::uint64_t> five;

    // This step, filling the vectors, could have been parallized, but it takes
    // next to no time to execute.
    std::vector<std::vector<std::uint64_t>*> bucket{ &one, &two, &three, &four, &five };
    for (auto i : bucket) {
        i == &four ? fill(*i, givenSize, givenSize * 2, prng) : fill(*i, givenSize, maxValue, prng);
    }

    // For smaller vector sizes, executing serially ensures proper printing, and
    // the sorts are so fast that it might be slower to create threads. This
    // changes whether we are running single or multi-threaded based on the
    // input size with 100,000 as the cut-off
    auto launchPolicy = givenSize < 100'000 ? std::launch::deferred : std::launch::async;

    // std::async returns a future<> object. There's a lot to futures, but all
    // we're going to care about right now is what the call looks like. The
    // first argument is the launchPolicy. You can specify that you need another
    // thread, or to hold off on evaluation until you try to get() the result
    // (lazy-evaluation). The next argument is the function you want to
    // evaluate. The following arguments are the arguments to the function you
    // want evaluated.
    auto insertion =
      std::async(launchPolicy, sort_container, "INSERTION", std::ref(one), insertionSort);
    auto merge = std::async(launchPolicy, sort_container, "MERGE", std::ref(two), mergeSort);
    auto selection =
      std::async(launchPolicy, sort_container, "SELECTION", std::ref(three), selectionSort);
    auto counting =
      std::async(launchPolicy, sort_container, "COUNTING", std::ref(four), countingSort);
    auto heap = std::async(launchPolicy, sort_container, "HEAP", std::ref(five), heapSort);

    // std::async, can specify a period to wait before automatically killing the
    // thread. We are not taking advantage of that feature, just waiting for
    // each thread to finish.
    insertion.wait();
    merge.wait();
    selection.wait();
    counting.wait();
    heap.wait();

    return 0;
}

void conditional_print(const std::vector<std::uint64_t>& container)
{
    if (container.size() <= 20) {
        for (auto i : container)
            std::cout << i << "  ";
        std::cout << '\n';
    }
}

void fill(std::vector<std::uint64_t>& a,
          std::uint64_t               size,
          std::uint64_t               maxValue,
          std::mt19937&               prng)
{
    a.reserve(size);

    std::uniform_int_distribution<unsigned int> dist(1, maxValue);

    for (int i = 0; i < size; i++) {
        a.push_back(dist(prng));
    }
}

void sort_container(std::string                 sortAlgo,
                    std::vector<std::uint64_t>& v,
                    void (*sortFunc)(std::vector<std::uint64_t>&))
{
    {
        // lock_guard() is a simple RAII method of ensuring exclusive execution.
        // Here I created scope where I want to guarantee one and only one
        // thread can execute at a time, and used the global mutex to lock it
        // down. When the scope ends, the lock is automatically freed.
        std::lock_guard<std::mutex> locker(m);
        std::cout << "\n*** "
                  << "Starting " << sortAlgo << " Sort, Thread: " << std::this_thread::get_id()
                  << " ***\n";
        conditional_print(v);
    }

    Timer t;
    t.start();
    sortFunc(std::forward<decltype(v)>(v));
    t.stop();
    {
        std::lock_guard<std::mutex> locker(m);
        conditional_print(v);
        std::cout << "It took " << t.milliseconds() << " milliseconds for " << sortAlgo
                  << " sort!\n";
    }
}
