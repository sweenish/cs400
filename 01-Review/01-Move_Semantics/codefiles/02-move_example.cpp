#include <iostream>

template <class T>
class Auto_ptr {
    T* m_ptr = nullptr;

public:
    Auto_ptr() { std::cout << "Auto_ptr default ctor\n"; }

    Auto_ptr(T* ptr)
      : m_ptr(ptr)
    {
        std::cout << "Auto_ptr parameterized ctor\n";
    }

    ~Auto_ptr()
    {
        std::cout << "Auto_ptr destructor\n";
        if (m_ptr)
            delete m_ptr;
    }

    // Copy constructor
    // Do deep copy of a.m_ptr to m_ptr
    Auto_ptr(const Auto_ptr& a)
      : m_ptr(new T(*a.m_ptr))
    {
        std::cout << "Auto_ptr Copy Constructor\n";
    }

    // Move constructor
    // Transfer ownership of a.m_mptr to m_ptr
    Auto_ptr(Auto_ptr&& a)
      : Auto_ptr()
    {
        std::cout << "Auto_ptr Move Constructor\n";
        swap(*this, a);
    }

    // Copy assignment
    // Do deep copy of a.m_ptr to m_ptr
    Auto_ptr& operator=(Auto_ptr a)
    {
        std::cout << "Auto_ptr Assignment\n";
        swap(*this, a);
        return *this;
    }

    T& operator*() const { return *m_ptr; }
    T* operator->() const { return m_ptr; }
       operator bool() const { return m_ptr != nullptr; }

    friend void swap(Auto_ptr<T>& lhs, Auto_ptr<T>& rhs) { std::swap(lhs.m_ptr, rhs.m_ptr); }
};

class Resource {
public:
    Resource() { std::cout << "Resource acquired\n"; }
    ~Resource() { std::cout << "Resource destroyed\n"; }
};

Auto_ptr<Resource> generateResource()
{
    Auto_ptr<Resource> res(new Resource);
    return res;  // this return value will invoke the move constructor
}

int main()
{
    std::cout << "*** In main()\n";
    Auto_ptr<Resource> mainres;
    std::cout << "*** Leaving main()\n";
    mainres = generateResource();  // this will invoke the move assignment operator
    std::cout << "*** Back in main()\n";
    std::cout << "*** End program\n";

    return 0;
}

/*
 * Running this code shows that the program runs much more efficiently. Only one resource is
 * acquired, and only one resource is destroyed, which is really all we wanted from the code.
 */