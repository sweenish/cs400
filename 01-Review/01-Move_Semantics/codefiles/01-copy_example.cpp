#include <iostream>
#include <utility>

// "Smart" pointer class declared and implemented in palce
template <class T>
class Auto_ptr {
    T* m_ptr = nullptr;  // Private data; classes are private by default

public:
    Auto_ptr() { std::cout << "Auto_ptr default ctor\n"; }

    Auto_ptr(T* ptr)
      : m_ptr(ptr)
    {
        std::cout << "Auto_ptr parameterized ctor\n";
    }

    ~Auto_ptr()
    {
        std::cout << "Auto_ptr destructor\n";
        delete m_ptr;
    }

    // Copy constructor
    // Do deep copy of a.m_ptr to m_ptr
    Auto_ptr(const Auto_ptr& a)
      : m_ptr(new T(*a.m_ptr))
    {
        std::cout << "Auto_ptr Copy Constructor\n";
    }

    // Copy assignment
    // Do deep copy of a.m_ptr to m_ptr
    Auto_ptr& operator=(Auto_ptr a)
    {
        std::cout << "Auto_ptr Copy Assignment\n";
        swap(*this, a);

        return *this;
    }

    T& operator*() const { return *m_ptr; }
    T* operator->() const { return m_ptr; }
       operator bool() const { return m_ptr != nullptr; }

    friend void swap(Auto_ptr<T>& lhs, Auto_ptr<T>& rhs) { std::swap(lhs.m_ptr, rhs.m_ptr); }
};

// Another class to illustrate our point
class Resource {
public:
    Resource() { std::cout << "Resource acquired\n"; }
    ~Resource() { std::cout << "Resource destroyed\n"; }
};

// A non-class function
Auto_ptr<Resource> generateResource()
{
    std::cout << "*** In generateResource()\n";
    Auto_ptr<Resource> res(new Resource);  // this will invoke the parameterized constructor
    return res;                            // this will invoke the copy constructor
}

int main()
{
    std::cout << "*** In main()\n";
    Auto_ptr<Resource> mainres;
    std::cout << "*** Leaving main()\n";
    mainres = generateResource();  // this assignment will invoke the copy assignment
    std::cout << "*** Back in main()\n";
    std::cout << "*** End program\n";

    return 0;
}

/*
 * Running the code, we can observe a surprising amount of extra work going on in the background.
 * Notably in the function generateResource(). What's happening is that the compiler is making all
 * the necessary copies that we have required in our code. This serves to illustrate how inefficient
 * copy semantics alone can be.
 */