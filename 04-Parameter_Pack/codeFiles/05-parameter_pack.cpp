#include <iostream>
#include <string>

/*
 * This program fixes one downside of initializer lists.
 */

// Base case
template <typename T>
std::ostream& print(std::ostream& sout, const T& rhs);

// Recursive case
template <typename T, typename... Args>
std::ostream& print(std::ostream& sout, const T& first, const Args&... rest);

int main()
{
    // Directly calls base case
    print(std::cout, "Hello world!");
    std::cout << std::endl;

    // The pack contains 5 items, the first string goes to the T type
    // Creates print(ostream&, string&, int&, string&, double&, string&, char&)
    print(std::cout, "What we have are ", 5, " to ", 7.5, " instances of Agent ", 'U');
    std::cout << std::endl;


    return 0;
}

template <typename T>
std::ostream& print(std::ostream& sout, const T& rhs)
{
    return sout << rhs;
}

template <typename T, typename... Args>
std::ostream& print(std::ostream& sout, const T& first, const Args&... rest)
{
    sout << first;
    return print(sout, rest...);  // Pack expansion
}


/*
 * The Parameter Pack
 *
 * We see the declaration of the parameter pack in line 11.
 * What that line is doing is declaring "two" template parameter types. The
 * first is what we've always seen, a type parameter we call T, that can
 * assume any single type.
 *
 * The second, typename ...Args, is the interesting bit. The ... says that
 * we will have a variable number of "things," in our case typename parameters.
 * The name is Args is not mandatory, but very common. Another common name is Ts
 *
 * On line 12, what we see is called pack expansion. What we are expanding here
 * are all the type parameters. A parameter pack can have zero to however many
 * types. To expand a pack, we place the ... on the right. There are a couple
 * different ways to expand the pack, we'll see the other way in the following
 * program.
 *
 * How the Recursion Works
 * Note that in line 41, we have a recursive call, but we only pass two
 * parameters; the function takes three. What happens is that the first member
 * of the pack is stripped away and applied to the second parameter of type T.
 * The remaining members of the pack are passed as the pack. Every time the
 * recursive call occurs, the pack shrinks by one.
 *
 * When we arrive to a case where all we are passing is the stream and one
 * other item, both functions qualify. This is because the parameter pack can
 * contain zero items. The base case function will always be used,
 * because it is more specialized than the version with the parameter pack.
 */