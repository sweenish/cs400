#include <iostream>

int add(int a);
int add(int a, int b);
int add(int a, int b, int c);
int add(int a, int b, int c, int d);

int main()
{
    std::cout << add(1) << '\n';
    std::cout << add(1, 2) << '\n';
    std::cout << add(1, 2, 3) << '\n';
    std::cout << add(1, 2, 3, 4) << '\n';

    return 0;
}

int add(int a)
{
    return a;
}

int add(int a, int b)
{
    return a + b;
}

int add(int a, int b, int c)
{
    return a + b + c;
}

int add(int a, int b, int c, int d)
{
    return a + b + c + d;
}

/*
 * As we can see, we have a relatively simple algorithm, and the number of
 * parameters really doesn't change things for us. However, we are currently
 * forced to provide an overload every time we need another parameter added to
 * the list.
 *
 * What are some possible solutions that we could write to mitigate this
 * code repition?
 */