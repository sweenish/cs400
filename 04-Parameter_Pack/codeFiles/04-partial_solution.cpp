#include <initializer_list>
#include <iostream>
#include <string>

/*
 * This program fixes one downside of initializer lists.
 */

// Add any type together
template <typename T>
T add(std::initializer_list<T> p);

int main()
{

    std::cout << add({ 1, 2, 3, 4, 5 }) << '\n';
    std::cout << add({ 1.1, 2.1, 3.1, 4.1, 5.1 }) << '\n';
    std::cout << add({ static_cast<std::string>("Hi. "), static_cast<std::string>("Hello. "),
                       static_cast<std::string>("How are you?") })
              << '\n';


    return 0;
}

template <typename T>
T add(std::initializer_list<T> p)
{
    T sum = T();
    for (auto i : p)
        sum += i;

    return sum;
}

/*
 * If one downside of initializer lists is that we lock ourselves into one
 * type, we know a way to fix that. We can template the function, and now we
 * are able to add any type that has a working += operator (changing the
 * function to just use + instead would be a bit more generic), but we are
 * still locked into only accepting a single type.
 *
 * In order to fully emulate emplace_back(), though, we also need to be able
 * to accept different types.
 *
 * This will require learning one more thing, as we don't have the tools right
 * now to fully solve this problem.
 */