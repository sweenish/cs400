#include <initializer_list>
#include <iostream>

/*
 * Before looking at how exactly how the emplace_back() function is able to
 * work, we'll look at someting a little simpler first.
 */

// Add any number of integers together
int add(std::initializer_list<int> p);

int main()
{
    std::cout << add({ 1 }) << '\n';
    std::cout << add({ 1, 2 }) << '\n';
    std::cout << add({ 1, 2, 3 }) << '\n';
    std::cout << add({ 1, 2, 3, 4 }) << '\n';
    std::cout << add({ 1, 2, 3, 4, 5 }) << '\n';
    std::cout << add({ 1, 2, 3, 4, 5, 6 }) << '\n';
    std::cout << add({ 1, 2, 3, 4, 5, 6, 7 }) << '\n';
    std::cout << add({ 1, 2, 3, 4, 5, 6, 7, 8 }) << '\n';

    return 0;
}

int add(std::initializer_list<int> p)
{
    int sum = 0;
    for (auto i : p)
        sum += i;

    return sum;
}

/*
 * Generally speaking, an initializer list refers to many objects of the
 * same type. It is like an array.
 *
 * It is like an array in that it has access to all the elements of the list,
 * but we can't substitute it for an array. There is no [] overload. In fact,
 * the class itself only consists of:
 *  - constructor
 *  - size()
 *  - begin()
 *  - end()
 *
 * This is because it has a very specific purpose, and that is to provide a
 * variable amount of parameters to a function. An initializer list should not
 * be thought of as a container. It does not contain the elements, it merely
 * references them. This means that a copy of an initializer list does not
 * create two lists with two sets of data. Both lists would refer to the same
 * underlying list.
 *
 * The values in an initializer list are also all const. Using an initializer
 * list still has some disadvantages. I would be required to have all my data
 * types be identical. Note I also had to enclose my function parameters in {}
 * in order to first construct the initializer list before calling the function.
 *
 * Care should be taken not to confuse an initializer list with a constructor
 * initialization list, what I call the initialization section.
 */