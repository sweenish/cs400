#include <cmath>    // round()
#include <iomanip>  // std::setprecision()
#include <iostream>
#include <string>
#include <utility>  // std::move(), std::forward()
#include <vector>

// A quick and dirty class to illustrate the main point of this file; having a
// ton of different constructors was the main idea
class Stuff {
public:
    Stuff()
      : dollars(0)
      , cents(0)
      , why(std::string())
    {}
    Stuff(int d)
      : dollars(d)
      , cents(0)
      , why(std::string())
    {}
    Stuff(int d, int c)
      : dollars(d)
      , cents(c)
      , why(std::string())
    {}
    Stuff(int d, int c, std::string w)
      : dollars(d)
      , cents(c)
      , why(w)
    {}
    Stuff(double m)
      : why(std::string())
    {
        create(m);
    }
    Stuff(double m, std::string w)
      : why(w)
    {
        create(m);
    }
    friend std::ostream& operator<<(std::ostream& sout, const Stuff& rhs)
    {
        sout << "$" << rhs.dollars << ".";
        if (rhs.cents < 10)
            sout << "0";
        sout << rhs.cents;

        sout << " for ";
        if (rhs.why == "")
            sout << "nothing.";
        else
            sout << rhs.why << ".";

        return sout;
    }

private:
    int         dollars;
    int         cents;
    std::string why;

    void create(double d)
    {
        int m = round(d * 100);

        dollars = m / 100;
        cents   = m % 100;
    }
};


template <typename T>
class List {
public:
    List();
    template <typename... Args>
    void   emplace_back(Args&&... args);
    size_t size() const;
    T&     operator[](size_t i);

private:
    T*     _arr;
    size_t _size;
    size_t _capacity;

    void grow();
};


int main()
{
    List<Stuff> lst;

    lst.emplace_back();
    lst.emplace_back(25);
    lst.emplace_back(3, 50);
    lst.emplace_back(3, 50, "dinosaurs");
    lst.emplace_back(4.24);
    lst.emplace_back(4.24, "a small piece of cheese");

    for (size_t i = 0; i < lst.size(); i++)
        std::cout << lst[i] << '\n';


    return 0;
}


template <typename T>
List<T>::List()
  : _arr(new T[0])
  , _size(0)
  , _capacity(0)
{}

template <typename T>
template <typename... Args>
void List<T>::emplace_back(Args&&... args)
{
    if (_size == _capacity)
        grow();

    _arr[_size] = T(std::forward<Args>(args)...);

    ++_size;
}

template <typename T>
size_t List<T>::size() const
{
    return _size;
}

template <typename T>
T& List<T>::operator[](size_t i)
{
    return _arr[i];
}

template <typename T>
void List<T>::grow()
{
    if (_capacity == 0) {
        delete[] _arr;
        _arr      = new T[1];
        _capacity = 1;
    } else {
        T* temp = new T[_capacity * 2];
        for (size_t i = 0; i < _size; i++)
            temp[i] = std::move(_arr[i]);

        delete[] _arr;
        _arr = temp;
        _capacity *= 2;
    }
}

/*
 * This program includes a very simple array list, but we've combined our
 * knowledge of templates, r-values, forwarding, and now the parameter pack to
 * re-create the function emplace_back()
 *
 * There is one new thing in the code as well. On line 94, note the placement
 * of the parameter pack ... ; it's outside the forward function, and not
 * directly attached to the pack variable rest.
 *
 * Where we choose to expand makes a difference. Had it been directly attached
 * to rest, then a call to forward would contain in its parameter list the
 * fully expanded pack. This would have caused a compile error, since forward
 * only accepts a single parameter.
 *
 * Instead, in placing it like so: std::forward<Args>(rest)...
 * we instead call forward as many times as necessary so that each member of
 * the pack is placed in a call to forward()
 *
 * To illustrate:
 * std::forward<Args>(rest...) --> std::forward<Args>(i1, i2, i3, ...)
 * std::forward<Args>(rest)... --> std::forward<T1>(i1), std::forward<T2>(i2)...
 *
 * Personally, I think this is pretty awesome. There aren't a ton of
 * opportunities in these classes to bring so many different principles and
 * aspects together so tightly to create such a robust solution. It's neat.
 */