#include <cmath>    // round()
#include <iomanip>  // std::setprecision()
#include <iostream>
#include <string>
#include <vector>

// A quick and dirty class to illustrate the main point of this file
class Stuff {
public:
    Stuff()
      : dollars(0)
      , cents(0)
      , why(std::string())
    {}
    Stuff(int d)
      : dollars(d)
      , cents(0)
      , why(std::string())
    {}
    Stuff(int d, int c)
      : dollars(d)
      , cents(c)
      , why(std::string())
    {}
    Stuff(int d, int c, std::string w)
      : dollars(d)
      , cents(c)
      , why(w)
    {}
    Stuff(double m)
      : why(std::string())
    {
        create(m);
    }
    Stuff(double m, std::string w)
      : why(w)
    {
        create(m);
    }
    friend std::ostream& operator<<(std::ostream& sout, const Stuff& rhs)
    {
        sout << "$" << rhs.dollars << ".";
        if (rhs.cents < 10)
            sout << "0";
        sout << rhs.cents;

        sout << " for ";
        if (rhs.why == "")
            sout << "nothing.";
        else
            sout << rhs.why << ".";

        return sout;
    }

private:
    int         dollars;
    int         cents;
    std::string why;

    void create(double d)
    {
        int m = round(d * 100);

        dollars = m / 100;
        cents   = m % 100;
    }
};


int main()
{
    std::vector<Stuff> vec;

    vec.emplace_back();
    vec.emplace_back(25);
    vec.emplace_back(3, 50);
    vec.emplace_back(3, 50, "dinosars");
    vec.emplace_back(4.24);
    vec.emplace_back(4.24, "a small piece of cheese");

    std::cout << '\n';

    // Trying the same with push_back()
    // Commented out lines will fail to compile
    // vec.push_back();         // Default money object fails
    vec.push_back(25);
    // vec.push_back(3, 50);
    // vec.push_back(3, 50, "dinosaurs");
    vec.push_back(4.24);
    // vec.push_back(4.24, "a small piece of cheese");

    for (auto i : vec)
        std::cout << i << '\n';


    return 0;
}

/*
 * The vector class has a very cool (technically it has a couple) function
 * called emplace_back(). As we can see in the main() above, it is not the
 * same as push_back(), since it will work in a few cases where push_back()
 * won't.
 *
 * push_back() requires an already constructed object to be pushed to the back
 * of the vector. The two lines that do work are because we take advantage of
 * implicit construction. We can not take advantage of implicit construction of
 * a default object, or utilize any constructor that requires more than one
 * parameter.
 *
 * emplace_back() takes as its parameters, constructor parameter lists. This
 * lecture will pick up that hanging thread from the forwarding lecture.
 */