/*
 * These pieces of code are from C++ 5th Edition, p. 692 - 694
 */
#include <iostream>

/*** Same as 02 ***/
void f(int v1, int& v2)
{
    std::cout << v1 << " " << ++v2 << std::endl;
}

/*** Extra function to demonstrate that there is still a problem ***/
//  void g(int &&i, int& j)
//  {
//     std::cout << i << " " << j << std::endl;
//  }

/*
 * Template takes a function as a parameter, and two extra parameters
 * The other two are flipped around when passed to the function
 *
 * This function now preserves things like const and references by using
 * r-values as function parameters
 */
template <typename F, typename T1, typename T2>
void flip(F f, T1&& t1, T2&& t2)
{
    f(t2, t1);
}


int main()
{
    int one = 42;

    flip(f, one, 55);
    std::cout << "one: " << one << std::endl;

    /*** This call will not compile ***/
    // flip(g, one, 55);
    // std::cout << "one: " << one << std::endl;

    return 0;
}

/*
 * Running this code may seem like we've cleard the final hurdle. We are
 * getting the behavior we expected. But we've only fixed it for l-value
 * references. If I have another function:
 *
 * void g(int &&i, int& j)
 * {
 *      std::cout << i << " " << j << std::endl;
 * } -*-
 *
 * and then attemp to use it with flip(), I will get a compile error.
 * flip(g, one, 42);    // Compile error
 *
 * It looks good. 42 is an r-value, and it will be passed as the first parameter
 * to g(), which also accepts an r-value. But that's not what will happen.
 *
 * What will happen is that in flip(), the r-value 42 is being passed to the
 * second parameter, t2. t2 is itself an l-value. So inside flip(), the
 * function will use t2, which is an l-value. And we will get a compile error.
 *
 * -*- The code is there, commented out. Uncomment it and see what happens and
 * what kind of error message you get.
 */