/*
 * These pieces of code are from C++ 5th Edition, p. 692 - 694
 */
#include <iostream>
#include <utility>  // std::forward<T>()

/*** Same as 02 ***/
void f(int v1, int& v2)
{
    std::cout << v1 << " " << ++v2 << std::endl;
}

/*** Extra function to demonstrate that everyting works now ***/
void g(int&& i, int& j)
{
    std::cout << i << " " << j << std::endl;
}

/*
 * Template takes a function as a parameter, and two extra parameters
 * The other two are flipped around when passed to the function
 *
 * This function now preserves things like const and references by using
 * r-values as function parameters
 *
 * This function also now perfectly preserves type value information as well
 */
template <typename F, typename T1, typename T2>
void flip(F f, T1&& t1, T2&& t2)
{
    f(std::forward<T2>(t2), std::forward<T1>(t1));
}


int main()
{
    int one = 42;

    flip(f, one, 55);
    std::cout << "one: " << one << std::endl;

    /*** This call now also works ***/
    flip(g, one, 55);
    std::cout << "one: " << one << std::endl;

    return 0;
}

/*
 * The forward() function allows us to fully preserve the type value, namely
 * r-values no longer get collapsed to l-values.
 *
 * This is considered perfect forwarding. There are a couple parts that we
 * have to keep in mind:
 *  - The parameters of the function that will do the forwarding must be
 *    r-value references to template types
 *  - You must use the forward() function
 */