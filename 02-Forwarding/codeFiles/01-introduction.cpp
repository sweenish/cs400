/*
 * These pieces of code are from C++ 5th Edition, p. 692 - 694
 */
#include <iostream>

void f(int v1, int v2)
{
    std::cout << v1 << " " << ++v2 << std::endl;
}

/*
 * Template takes a function as a parameter, and two extra parameters
 * The other two are flipped around when passed to the function
 *
 * This function does not preserve type information at all (const and references
 * are lost)
 */
template <typename F, typename T1, typename T2>
void flip(F f, T1 t1, T2 t2)
{
    f(t2, t1);
}


int main()
{
    int one = 42;

    flip(f, one, 55);
    std::cout << "one: " << one << std::endl;

    return 0;
}

/*
 * It seems like everything is working as expected, and for now they are. But
 * there are some deep-seated issues in this code. To see what they could be,
 * move on to the next program.
 */