/*
 * These pieces of code are from C++ 5th Edition, p. 692 - 694
 */
#include <iostream>

/*** We have changed the second parameter to int& from int ***/
void f(int v1, int& v2)
{
    std::cout << v1 << " " << ++v2 << std::endl;
}

/*
 * Template takes a function as a parameter, and two extra parameters
 * The other two are flipped around when passed to the function
 *
 * This function does not preserve type information at all (const and references
 * are lost)
 */
template <typename F, typename T1, typename T2>
void flip(F f, T1 t1, T2 t2)
{
    f(t2, t1);
}


int main()
{
    int one = 42;

    flip(f, one, 55);
    std::cout << "one: " << one << std::endl;

    return 0;
}

/*
 * Because of the change in f(), we may have expected the variable one to be
 * changed, but it was not.
 *
 * If we had called f() directly and given one as the second parameter, it
 * would behave as we expect. So what's going on?
 *
 * one is an l-value. When one is passed to flip1(), the template parameter
 * type T1 binds to int. That means that the parameter t1 (note capitalization)
 * will be a copy of one. That copy (t1) is passed to f() by reference, and
 * that copy (t1) is directly altered. Not quite what we would have expected,
 * and likely not what we wanted.
 */