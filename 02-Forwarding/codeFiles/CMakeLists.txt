cmake_minimum_required(VERSION 3.15)

project(forwarding)
set(CMAKE_CXX_STANDARD 14)

## Set variables
set(INTRO 01-introduction)
set(ISSUES 02-issues)
set(PARTIAL 03-partial_solution)
set(SOL 04-perfect_forwarding)

## Name executables
add_executable("${INTRO}" "${INTRO}.cpp")
add_executable("${ISSUES}" "${ISSUES}.cpp")
add_executable("${PARTIAL}" "${PARTIAL}.cpp")
add_executable("${SOL}" "${SOL}.cpp")