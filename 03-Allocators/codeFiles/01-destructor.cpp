#include <iostream>

// A very straightforward class to demonstrate that explicit destructor calls
// don't do what we think they do.
class SDClass {  // a simple dynamic class
public:
    SDClass(double& d)
      : dptr(new double(d))
    {
        std::cout << "CTOR CALLED\n";
    }
    friend std::ostream& operator<<(std::ostream& sout, const SDClass& rhs);
    ~SDClass();

private:
    double* dptr;
};

SDClass::~SDClass()
{
    std::cout << "DTOR CALLED\n";
    if (dptr != nullptr) {
        delete dptr;
        dptr = nullptr;
    }
}

std::ostream& operator<<(std::ostream& sout, const SDClass& rhs)
{
    return sout << *(rhs.dptr);
}

int main()
{
    double  pi = 3.1415926;
    SDClass obj(pi);

    std::cout << obj << std::endl;

    obj.~SDClass();  // The explicit destructor call we are focusing on

    return 0;
} /*
   * Running this code, we may observe something interesting. Notably, the
   * destructor gets called twice. There is only a single object, though.
   *
   * In calling the destructor explicitly, we will execute its code. It will not,
   * however, remove the object from memory like we would think. So right now,
   * obj still exists after the destructor call, but it has been gimped and is
   * currently unusable.
   *
   * On the one hand, we could re-configure our entire class to elegantly handle
   * the situation where a destructor was explicitly called...
   * Or we could just leave destruction up to the compiler; programmers pick this
   * option
   *
   * But this leads to an interesting problem
   * In learning how vectors work, the erase function contains some very specific
   * language:
   * "Removes from the vector either a single element (position) or a range of
   * elements ([first,last)).
   *
   * This effectively reduces the container size by the number of elements
   * removed, which are >>destroyed<<."
   *
   * That indicates that there is a way to explicitly destroy objects.
   * It goes hand-in-hand with the fact that a default constructed vector will
   * break if you try to access any elements using [] or at() before adding
   * elements. But our version doesn't actually behave that way, either. The two
   * are related.
   */