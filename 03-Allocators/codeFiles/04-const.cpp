#include <iostream>

int main()
{
    /*
     * This program is meant for experimentation
     * Note the different ways that const is used when declaring a pointer. Try
     * and guess which lines underneath each pointer declaration will and won't
     * work, and why. Then compile and see if you were right.
     */

    int one = 42;
    int two = 37;

    const int* p = &one;  // What we're most familiar with
    // *p = two;
    // p = &two;

    int const* q = &one;
    // *q = two;
    // q = & two;

    int* const r = &one;
    // *r = two;
    // r = &two;
}

/*
 * What we should be observing is that it matters where const is in regards to
 * the *, and not so much the type
 */