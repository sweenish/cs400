#include <iostream>
#include <memory>  // std::allocator class
#include <string>

/*
 * This program will show the very beginnings of using allocators to better
 * impersonate a vector
 */

class PFAString {
public:
    PFAString();
    PFAString(std::string first);
    PFAString(size_t cap, std::string str = std::string());
    ~PFAString();
    size_t             size() const noexcept;
    size_t             capacity() const noexcept;
    void               push_back(const std::string& str);
    std::string&       operator[](size_t index);
    const std::string& operator[](size_t index) const;

private:
    static std::allocator<std::string> alloc;
    using allocTraits = std::allocator_traits<decltype(alloc)>;

    std::string* begin_;
    std::string* end_;
    std::string* cap_;
    int          size_;
    int          capacity_;

    void grow();
};

/*** Static Initialization ***/
std::allocator<std::string> PFAString::alloc;


/*** Non-class function ***/
void print(const PFAString& obj)
{
    for (size_t i = 0; i < obj.size(); i++)
        std::cout << obj[i] << " ";
    std::cout << '\n' << "Size: " << obj.size() << '\n';
    std::cout << "Capacity: " << obj.capacity() << "\n\n";
}


/*** main() ***/
int main()
{
    PFAString simple;
    print(simple);

    simple.push_back("What");
    print(simple);

    simple.push_back("does");
    print(simple);

    simple.push_back("the");
    print(simple);

    simple.push_back("fox");
    print(simple);

    simple.push_back("say?");
    print(simple);
}


/*** PFAString implementation ***/
PFAString::PFAString()
  : begin_(nullptr)
  , end_(nullptr)
  , cap_(nullptr)
  , size_(0)
  , capacity_(0)
{}

PFAString::PFAString(std::string first)
  : begin_(allocTraits::allocate(alloc, 1))
  , end_(begin_)
  , cap_(begin_ + 1)
  , size_(1)
  , capacity_(1)
{
    allocTraits::construct(alloc, end_++, first);
}

PFAString::PFAString(size_t cap, std::string str)  // Second parameter optional; default ctor
  : begin_(alloc.allocate(cap))
  , end_(begin_)
  , cap_(begin_ + cap)
  , size_(cap)
  , capacity_(cap)
{
    for (size_t i = 0; i < cap; i++)
        allocTraits::construct(alloc, end_++, str);
}

PFAString::~PFAString()
{
    if (begin_) {
        while (end_ != begin_) {
            allocTraits::destroy(alloc, --end_);
        }
        allocTraits::deallocate(alloc, begin_, capacity_);
    }
    begin_ = end_ = cap_ = nullptr;
}

size_t PFAString::size() const noexcept
{
    return size_;
}

size_t PFAString::capacity() const noexcept
{
    return capacity_;
}

void PFAString::push_back(const std::string& str)
{
    if (size_ == capacity_) {
        grow();
    }

    allocTraits::construct(alloc, end_++, str);
    size_ = end_ - begin_;
}

std::string& PFAString::operator[](size_t index)
{
    return begin_[index];
}

const std::string& PFAString::operator[](size_t index) const
{
    return begin_[index];
}


void PFAString::grow()
{
    if (begin_ == nullptr) {
        begin_    = alloc.allocate(1);
        end_      = begin_;
        cap_      = begin_ + 1;
        capacity_ = cap_ - begin_;
        return;
    }

    capacity_ *= 2;
    std::string* temp    = allocTraits::allocate(alloc, capacity_);
    std::string* tempEnd = temp;

    // Since the old array is getting destroyed anyway, we can safely move the
    // data out of the old array into the new one
    for (std::string* i = begin_; i != end_; i++) {
        allocTraits::construct(alloc, tempEnd++, std::move(*i));
    }

    while (end_ != begin_) {
        allocTraits::destroy(alloc, --end_);
    }

    allocTraits::deallocate(alloc, begin_, capacity_);

    begin_ = temp;
    end_   = tempEnd;
    cap_   = begin_ + capacity_;
}
