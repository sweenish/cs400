/*
 * This file serves to illustrate the right left rule, and show how the
 * placement of const can alter what is made const
 */

int main()
{
    int first  = 42;
    int second = 37;

    int const* pi_one = &first;
    // pi_one = &second;
    // *pi_one = 55;

    int* const pi_two = &second;
    // pi_two = &first;
    // *pi_two = 55;

    return 0;
}

/*
 * These two pointers are a bit different from each other. The way to properly
 * determine what each variable actually is, is called the right left rule.
 *
 * http://andybohn.com/deciphering-variable-types/
 *
 * All it says to do is start with the variable for which you are determining
 * the type. Then read to right, then to the left, turning the symbols into words.
 *
 * char * str[10]
 *
 * str is an...
 * We to the right first from `str' and the first character we see is a `[' so, that means we have
 * an array, so...
 *  - "str is an array 10 of...
 * We've read everything to the right, now we start going left, and the next thing we encounter is
 * the `*', that means we have pointers, so...
 *  - "str is an array 10 of pointers to...
 * Keep going left and we get to the type 'char', so...
 *  - "str is an array 10 of pointers to char"
 * We have finished our sweeps; therefore we are done!
 *
 * Whenever see a gnarly looking complex type, we can always figure out what it
 * is by applying this rule.
 */