#include <iostream>
#include <memory>  // std::allocator class;
// http://www.cplusplus.com/reference/memory/allocator/?kw=allocator

/*
 * Allocators are a template class that allow the programmer to separate the
 * allocation of memory from the heap and object construction
 *
 * The class is designed in such a way that you would typically declare a
 * static allocator object in your class, and that single object will be
 * responsible for acquiring and returning memory to the heap
 *
 * Because allocators get us heap memory "in the raw," we will also need to use
 * the allocator to construct AND destroy objects in the allocated memory
 */

int main()
{
    // We'll keep the data type simple so we can see these addresses
    std::allocator<int> alloc;  // An allocator object that deals with ints

    // We'll talk about this line of code after the main(); the takeaway is
    // is that I used the allocator to get raw memory that can hold 5 ints
    int* const arr = alloc.allocate(5);

    // If arr is going to be used to track where the array starts, end will
    // track our used elements. In other words, end will always point to one
    // past the last existing element
    int* end = arr;

    for (size_t i = 0; i < 3; i++)
        alloc.construct(end++, i + 1);
    // We are only going to construct in some of the total space available to us
    // The first parameter is the location in allocated memory where we wish to
    // construct an object
    // After the first parameter, we can have 0 to n more parameters, which
    // will be used to construct our object. In the case of an int, there can be
    // at most one parameter. How we are able to have a variable number of
    // parameters will be discussed soon


    // Pointer arithmetic to determine the size (active elements) of the array
    size_t size = end - arr;

    for (size_t i = 0; i < size; i++)
        std::cout << &(arr[i]) << ": " << arr[i] << '\n';

    // After working with the dynamic array, we still have to return the memory
    // In the same way that new had two jobs, delete also has two jobs; delete
    // destroys objects and returns memory
    while (end != arr)
        alloc.destroy(--end);  // We are destroying from back to the front

    alloc.deallocate(arr, 5);

    return 0;
}

/*
 * ### About the line: int * const arr = al.allocate(5);
 * Just when you thought you were done learning about const...
 * Actually you are. What const does never changes, what we're actually going to
 * learn is how to read what arr is
 * http://andybohn.com/deciphering-variable-types/
 *
 * Using the right left rule, we will basically read what arr is by sweeping right,
 * then left from arr
 * So...
 * arr is a const pointer to an int
 *
 * To contrast, if the line were: int const * arr = alloc.allocate(5);
 *                    Also valid: const int * arr = alloc.allocate(5);
 * We would read it as...
 * arr is a pointer to an int that is constant (or a pointer to a const int)
 *
 * There is a difference between the two. The best example is probably to
 * remember that an array is a pointer in disguise. But an array pointer is also
 * a bit special, in that once assigned, the array variable must point to the
 * same thing at all times. It's impossible to assign the pointer to point to
 * anything else
 *
 * This means that in the line of code in main(), arr is mimicking the type of
 * pointer that an array is
 */

/*
 * ### About allocation.
 * We can see that when compared to new and delete, we require more lines of
 * code to do the same things. This may seem pointless, but when cycles matter,
 * this code is better.
 *
 * We only constructed 3 integers in the space we allocated, and we constructed
 * them only when we needed them.
 * - So, 3 objects were needed, 3 were constructed, and 3 were destroyed
 *
 * In the case of new and delete, the array would have had all elements default-
 * initialized, and we would not have used two of the elements. Furthermore, we
 * re-constructed the elements we actually cared about
 * - So, 3 objects were needed, 8 were constructed, and 5 were destroyed
 *
 * Hopefully it is easy to see how poorly new and delete would scale in larger
 * partially filled arrays.
 *
 * This code is also more versatile in that it will work with classes that do
 * not have default constructors
 *
 * Final note on allocators: It's a good class to use because it retrieves the
 * correct amount of memory for our type, and that memory is ALIGNED
 *
 * To understand memory alignment, we need to understand how the CPU accesses
 * memory. It can only really access memory one WORD at a time. On modern
 * systems, that can generally be seen as 64 bits (8 bytes)
 *
 * Putting it simply, type-aligned memory minimizes the number of memory accesses
 * required to read an object from memory
 */