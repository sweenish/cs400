#include <iostream>
#include <string>

int main()
{
    int*         iptr = new int;
    double*      dptr = new double;
    std::string* sptr = new std::string;


    std::cout << *iptr << '\n' << *dptr << '\n' << "*" << *sptr << "*" << '\n';

    delete iptr;
    delete dptr;
    delete sptr;
}

/*
 * On the one hand, it's probably not surprising that this code compiles and
 * actually has output
 *
 * But if we look deeper at what that output means, we will realize that the
 * keyword new is doing more than just getting memory from the heap
 *
 * The keyword new will also invoke a constructor call. In the code above, all
 * pointers utilized the default constructors for the their respective type. So
 * this means that the keyword new is getting constructing an object onto the
 * heap and THEN returning the address of the constructed object.
 *
 * What this means is that when we get a 'new' array, we are invoking the
 * default constructor for every element of the array. This is generally
 * wasteful. If we don't use a spot, we have constructed an object for nothing.
 * If we do use an index spot, we immediately overwrite it with our own data,
 * typically through a new construction.
 *
 * It also means that if a class does not have a default constructor, we cannot
 * get a dynamic array of that type
 *
 * What we need is a way to separate the allocation of memory from the
 * construction of an object
 */