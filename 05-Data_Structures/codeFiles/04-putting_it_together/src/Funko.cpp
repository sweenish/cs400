#include "Funko.hpp"

Funko::Funko(std::string name)
  : name_m(name)
{}

std::string Funko::info_main() const
{
    return name_m + " Funko Pop\n";
}