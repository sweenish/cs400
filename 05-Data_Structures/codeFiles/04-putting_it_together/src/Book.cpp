#include "Book.hpp"

Book::Book(std::string sur, std::string fore, std::string title, int year)
  : Shelvable()
  , surname_m(sur)
  , forename_m(fore)
  , title_m(title)
  , year_m(year)
{}

std::string Book::surname() const
{
    return surname_m;
}

std::string Book::forename() const
{
    return forename_m;
}

std::string Book::title() const
{
    return title_m;
}

int Book::year() const
{
    return year_m;
}
