#include "Novel.hpp"

Novel::Novel(std::string sur, std::string fore, std::string title, int year)
  : Book(sur, fore, title, year)
{}

std::string Novel::info_main() const
{
    return "Title: " + title() + "\nAuthor: " + forename() + " " + surname() +
           "\nYear Published: " + std::to_string(year()) + "\n";
}
