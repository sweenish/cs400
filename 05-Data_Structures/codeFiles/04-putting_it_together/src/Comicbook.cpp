#include "Comicbook.hpp"

Comicbook::Comicbook(std::string sur, std::string fore, std::string title, int issue, int year)
  : Book(sur, fore, title, year)
  , issue_m(issue)
{}

std::string Comicbook::info_main() const
{
    return "Title: " + title() + "\nIssue: " + std::to_string(issue()) + "\nAuthor: " + forename() +
           " " + surname() + "\nYear Published: " + std::to_string(year()) + "\n";
}

int Comicbook::issue() const
{
    return issue_m;
}