#include "Comicbook.hpp"
#include "Funko.hpp"
#include "Novel.hpp"
#include "ArrayList.hpp"
#include "Shelvable.hpp"
#include "Spacer.hpp"
#include <iostream>
#include <utility>

int main()
{
    using Shelf = ArrayList<Shelvable*>;
    Shelf shelf;
    shelf.emplace_end(new Novel("Follet", "Ken", "The Pillars of the Earth", 1989));
    shelf.emplace_end(new Spacer());
    shelf.emplace_end(new Comicbook("Shuster", "Joe", "Action Comics", 1, 1938));
    shelf.emplace_end(new Spacer());
    shelf.emplace_end(new Funko("Wonder Woman"));

    for (auto i : shelf) {
        std::cout << i->info() << '\n';
    }

    std::cout << "### This is a divider ###\n\n";

    // Doing the same thing "manually"
    for (auto i = shelf.begin(); i != shelf.end(); ++i) {
        std::cout << (*i)->info() << '\n';
    }
}

/*
 * With the right OOP principles in play, we can now create a data structure
 * to simulate our bookshelf, and place typical "shelvable" objects on to it and
 * not just objects of a single type on a shelf.
 *
 * The free store (heap) memory is handled directly by our allocator.
 *
 * The new things like move semantics, perfect forwarding, and the paramter
 * pack allow us to directly construct objects into the container.
 *
 * If you've seen the explanation of how an arrow operator works, you might
 * rightfully be wondering why I have to bother dereferencing the shelf
 * iterator. By dereferencing the iterator first, note that I am skipping the
 * iterator arrow operator overload altogether.
 *
 * The iterator holds a T*, and for the object shelf, T is Shelvable*. The drill
 * down would return a Shelvable** (T*), which gets dereferenced by the arrow
 * operator. It would then attempt to apply the dot operator and fail to do so
 * because the type is still a pointer. This means the only way to correctly
 * call member functions on the shelf object is to dereference the iterator,
 * which gives me a T (Shelvable*), and use the built-in arrow operator on a
 * pointer.
 *
 * It is common practice for the iterator arrow overload to return T*, it
 * purposefully ends the drill down chain.
 *
 * The next file demonstrates the drill down of an arrow operator.
 */