#include "Shelvable.hpp"

std::string Shelvable::info() const
{
    std::string info = info_main();

    return info.size() == 0 ? "Doesn't look like anything to me.\n" : info;
}