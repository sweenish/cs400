#ifndef SHELVABLE_INTERFACE
#define SHELVABLE_INTERFACE

#include <string>

class Shelvable {
public:
    std::string info() const;

private:
    virtual std::string info_main() const = 0;
};

#endif