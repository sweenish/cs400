#ifndef BOOK_INTERFACE
#define BOOK_INTERFACE

#include "Shelvable.hpp"
#include <string>

class Book : public Shelvable {
public:
    Book(std::string sur, std::string fore, std::string title, int year);
    std::string surname() const;
    std::string forename() const;
    std::string title() const;
    int         year() const;

private:
    virtual std::string info_main() const = 0;

    std::string surname_m;
    std::string forename_m;
    std::string title_m;
    int         year_m;
};

#endif