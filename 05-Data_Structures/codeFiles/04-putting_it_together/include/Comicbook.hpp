#ifndef COMICBOOK
#define COMICBOOK

#include "Book.hpp"

class Comicbook : public Book {
public:
    Comicbook(std::string sur, std::string fore, std::string title, int issue, int year);
    int issue() const;

private:
    std::string info_main() const override;
    int         issue_m;
};

#endif