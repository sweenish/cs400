#ifndef FUNKO
#define FUNKO

#include "Shelvable.hpp"

class Funko : public Shelvable {
public:
    Funko(std::string name);

private:
    std::string info_main() const override;

    std::string name_m;
};

#endif