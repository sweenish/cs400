#ifndef SPACER
#define SPACER

#include "Shelvable.hpp"
#include <string>

class Spacer : public Shelvable {
public:
private:
    std::string info_main() const override;
};

#endif