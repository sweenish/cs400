#ifndef SHELF
#define SHELF

#include <iterator>
#include <utility>

/*
 * This is a very simple data structure to represent our bookshelf. The iterator
 * inherits from the simplest std::iterator. It gets much more complicated very
 * quickly.
 */
template <typename T>
class ArrayList {
public:
    ArrayList();
    ~ArrayList();
    ArrayList(const ArrayList& other);
    ArrayList(ArrayList&& other) noexcept(true);

    class iterator;
    iterator begin();
    iterator end();
    // Ideally we would also have const iterators and reverse iterators

    // This function is still the main purpose of this lecture
    template <typename... Args>
    void emplace_end(Args&&... pack);

    ArrayList& operator=(ArrayList other);
    ArrayList& operator=(ArrayList&& other) noexcept(true);

    friend void swap(ArrayList& lhs, ArrayList& rhs) noexcept(true);

private:
    static std::allocator<T> alloc;
    using allocTraits = std::allocator_traits<decltype(alloc)>;

    T* begin_m;
    T* end_m;
    T* capacity_m;

    void grow();
};

/*
 * Each function has a comment stating what requirement trait demands its
 * presence. These all stem from std::input_iterator_tag, i.e.,
 * std::input_iterator_tag corresponds to LegacyInputIterator which in turn
 * requires all of the other traits. Even this is more than is required to
 * simply get your data structure to work with a range-based for loop. It is at
 * least a taste of what goes in to a robust solution.
 *
 * The arrow operator overload is discussed in more detail at the bottom of this
 * file.
 */
template <typename T>
class ArrayList<T>::iterator : public std::iterator<std::input_iterator_tag, T> {
public:
    iterator(T* item);
    iterator(const iterator& other);                           // CopyConstructable
    ~iterator();                                               // Destructable
    T&             operator*();                                // LegacyIterator
    const T        operator*() const;                          // LegacyInputIterator
    iterator&      operator++();                               // LegacyIterator
    const iterator operator++(int);                            // LegacyInputIterator
    bool           operator==(const ArrayList<T>::iterator& rhs);  // EqualityComparable
    bool           operator!=(const ArrayList<T>::iterator& rhs);  // LegacyInputIterator
    T*             operator->();                               // LegacyInputIterator
    const T*       operator->() const;                         // LegacyInputIterator

    iterator& operator=(iterator other);  // CopyAssignable

    friend void swap(ArrayList<T>::iterator& lhs, ArrayList<T>::iterator& rhs);  // Swappable

private:
    T* item_m;
};

// Static initialization
template <typename T>
std::allocator<T> ArrayList<T>::alloc;

// ArrayList Functions
template <typename T>
ArrayList<T>::ArrayList()
  : begin_m(nullptr)
  , end_m(nullptr)
  , capacity_m(nullptr)
{}

template <typename T>
ArrayList<T>::~ArrayList()
{
    if (begin_m) {
        while (end_m != begin_m)
            allocTraits::destroy(alloc, --end_m);
        allocTraits::deallocate(alloc, begin_m, capacity_m - begin_m);
        begin_m    = nullptr;
        end_m      = nullptr;
        capacity_m = nullptr;
    }
}

template <typename T>
ArrayList<T>::ArrayList(const ArrayList& other)
{
    if (!other.begin_m) {
        begin_m    = nullptr;
        end_m      = nullptr;
        capacity_m = nullptr;
        return;
    }

    int capacity = other.capacity_m - other.begin_m;
    begin_m      = allocTraits::allocate(alloc, capacity);
    end_m        = begin_m;
    capacity_m   = begin_m + capacity;
    for (auto i = other.begin_m; i != other.end_m; ++i) {
        allocTraits::construct(alloc, end_m++, *i);
    }
}

template <typename T>
ArrayList<T>::ArrayList(ArrayList&& other) noexcept(true)
  : begin_m(other.begin_m)
  , end_m(other.end_m)
  , capacity_m(other.capacity_m)
{
    other.begin_m    = nullptr;
    other.end_m      = nullptr;
    other.capacity_m = nullptr;
}

template <typename T>
typename ArrayList<T>::iterator ArrayList<T>::begin()
{
    return iterator(begin_m);
}

template <typename T>
typename ArrayList<T>::iterator ArrayList<T>::end()
{
    return iterator(end_m);
}

template <typename T>
template <typename... Args>
void ArrayList<T>::emplace_end(Args&&... pack)
{
    if (begin_m == nullptr || end_m == capacity_m) {
        grow();
    }

    allocTraits::construct(alloc, end_m++, std::forward<Args>(pack)...);
}

template <typename T>
ArrayList<T>& ArrayList<T>::operator=(ArrayList other)
{
    swap(*this, other);
    return *this;
}

template <typename T>
ArrayList<T>& ArrayList<T>::operator=(ArrayList&& other) noexcept(true)
{
    swap(*this, std::move(other));
    return *this;
}

template <typename T>
void ArrayList<T>::grow()
{
    if (begin_m == nullptr) {
        begin_m    = allocTraits::allocate(alloc, 1);
        end_m      = begin_m;
        capacity_m = begin_m + 1;
        return;
    }

    int capacity = (capacity_m - begin_m) * 2;
    T*  tmp      = allocTraits::allocate(alloc, capacity);
    T*  tmpEnd   = tmp;
    for (auto i = begin_m; i != end_m; ++i) {
        allocTraits::construct(alloc, tmpEnd++, std::move(*i));
    }

    while (end_m != begin_m) {
        allocTraits::destroy(alloc, --end_m);
    }
    allocTraits::deallocate(alloc, begin_m, capacity_m - begin_m);
    begin_m    = tmp;
    end_m      = tmpEnd;
    capacity_m = begin_m + capacity;
}


// ArrayList iterator functions
template <typename T>
ArrayList<T>::iterator::iterator(T* item)
  : item_m(item)
{}

template <typename T>
ArrayList<T>::iterator::iterator(const iterator& other)
  : item_m(other.item_m)
{}

template <typename T>
ArrayList<T>::iterator::~iterator()
{
    item_m = nullptr;
}

template <typename T>
T& ArrayList<T>::iterator::operator*()
{
    return *item_m;
}

template <typename T>
const T ArrayList<T>::iterator::operator*() const
{
    return *item_m;
}

template <typename T>
typename ArrayList<T>::iterator& ArrayList<T>::iterator::operator++()
{
    ++item_m;
    return *this;
}

template <typename T>
const typename ArrayList<T>::iterator ArrayList<T>::iterator::operator++(int)
{
    iterator tmp = *this;
    ++(*this);

    return tmp;
}

template <typename T>
bool ArrayList<T>::iterator::operator==(const ArrayList<T>::iterator& rhs)
{
    return item_m == rhs.item_m;
}

template <typename T>
bool ArrayList<T>::iterator::operator!=(const ArrayList<T>::iterator& rhs)
{
    return !(*this == rhs);
}

template <typename T>
T* ArrayList<T>::iterator::operator->()
{
    return item_m;
}

template <typename T>
const T* ArrayList<T>::iterator::operator->() const
{
    return item_m;
}

template <typename T>
typename ArrayList<T>::iterator& ArrayList<T>::iterator::operator=(iterator other)
{
    swap(*this, other);
    return *this;
}

// Friend function
template <typename T>
void swap(ArrayList<T>& lhs, ArrayList<T>& rhs) noexcept(true)
{
    using std::swap;

    swap(lhs.begin_m, rhs.begin_m);
    swap(lhs.end_m, rhs.end_m);
    swap(lhs.capacity_m, rhs.capacity_m);
}

template <typename T>
void swap(typename ArrayList<T>::iterator& lhs, typename ArrayList<T>::iterator& rhs)
{
    using std::swap;

    swap(lhs.item_m, rhs.item_m);
}

#endif

/*
 * Arrow operator overload
 * Recall that an arrow operator does two things for us. It dereferences a
 * pointer, and invokes a member function using the dot operator. It is a
 * shorthand for (*objPtr).memberFunc(). So why does the overload look like
 * this:
 *
 * T* ArrayList<T>::iterator::operator->()
 *
 * Notice that the overload returns a pointer. If it's supposed to dereference,
 * why am I returning a pointer? The main reason is that the -> is a bit
 * special. It utlizes a "drill down" mechanism. What that means is that if the
 * first arrow retrieves an object that also has an arrow operator  overload, it
 * will automatically invoke it. It will continue to drill down until it reaches
 * a pointer type, at which point it will be automatically dereferenced. It
 * sounds weird, but it actually makes writing the overload for a class much
 * easier. You don't care if the pointer you're returning is to an object with
 * its own arrow overload. It doesn't matter. You just return the pointer you
 * need to, and if its the end of the chain, it works like you'd expect. If it's
 * not the end of the chain, it still works like you'd expect.
 */