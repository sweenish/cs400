#ifndef NOVEL
#define NOVEL

#include "Book.hpp"

class Novel : public Book {
public:
    Novel(std::string sur, std::string fore, std::string title, int year);

private:
    std::string info_main() const override;
};

#endif