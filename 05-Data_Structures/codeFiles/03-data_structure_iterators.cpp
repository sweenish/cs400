#include <iostream>
#include <utility>  // std::move()
#include <vector>

/*
 * This file is more concerned with the idea of what an iterator is (Also this
 * one will compile)
 *
 * An iterator is a standard way for users of our data structures to move
 * through the data. This is important because we navigate different data
 * structures in different ways. And instead of memorizing the specifics of
 * every data structure, we simply have to learn how to use an iterator. And
 * that's a lot easier because we will ALWAYS use iterators the same way
 * regardless of how the data is stored in our data structure.
 *
 * Now, as programmers, we also have to write iterators for our data structures
 * so that when it comes time to use it, we aren't surprised.
 *
 * This leads to an important note: the interface of an iterator is always the
 * same, but the implementation changes on a data structure by data structure
 * basis.
 */
template <typename T>
class ArrList {
public:
    ArrList();
    ArrList(const ArrList<T>& cop);
    ~ArrList();
    void   push_back(T item);
    size_t size() const;
    size_t capacity() const;

    /*
     * Here we forward declare the iterator class. This accomplishes a few
     * different things:
     * 1. It nests the iterator class inside of ArrList. This is desirable
     *      because we can't have an iterator without a data structure to
     *      iterate through. Iterators are also specific to their data structure
     * 2. It keeps our ArrList interface cleaner, since another class definition
     *      is not nested right here
     * 3. It makes iterator objects publicly accessible, since the forward
     *      declaration was made in the public section
     */
    class iterator;
    iterator begin() const;
    iterator end() const;

    ArrList<T>& operator=(const ArrList<T>& rhs);

private:
    T* _arr;

    size_t _size;
    size_t _capacity;

    void grow();
};

/*
 * Here is the declaration of the ArrList<T>::iterator class. The forward
 * declaration establishes this as a nested class within ArrList. Because the
 * iterator class is contained within ArrList, and ArrList is a template class,
 * we are required to have the template prefix. The iterator class itself is
 * NOT a template class.
 *
 * One other very important note: Because the iterator class is a class, it has
 * no concept of the private members of ArrList, another class. The nested
 * nature does not provide an exception to this rule.
 *
 * An iterator can be thought of as a generalization of a pointer. An iterator
 * should know the location of objects within our container. Iterators are
 * designed so that we can use it as if it was a pointer to the object in our
 * data structure.
 *
 * The iterator returned by ArrList<T>::begin() represents the "first" object
 * in our data structure. The iterator returned by ArrList<T>::end() represents
 * ONE PAST THE LAST object held by our data structure. In the case of our
 * array list, one past the last is the memory address following the last
 * occupied array element.
 *
 * If we wish to "activate" C++11 range-based for loops for our data structure,
 * the bare minimum that an iterator class must have are:
 *  - One constructor (should be obvious, but stated for completion)
 *  - An increment operator overload (one usually begets the other, so do both)
 *  - A de-reference operator overload
 *  - Equality and Inequality
 *      -- A note here: if you choose to use friend functions, implement them
 *          inline. Friends and templates are weird together. Throw in a nested
 *          class, and an inline implementation is the only sane thing to do.
 *
 * Iterators can be significantly more complicated than this if full STL
 * compatibility is desired.
 */
template <typename T>
class ArrList<T>::iterator {
public:
    iterator(T* loc);
    iterator&      operator++();
    const iterator operator++(int);
    T&             operator*();
    friend bool    operator==(const iterator& lhs, const iterator& rhs)
    {
        return lhs._indexLoc == rhs._indexLoc;
    }
    friend bool operator!=(const iterator& lhs, const iterator& rhs) { return !(lhs == rhs); }

private:
    T* _indexLoc;
};


// MAIN FUNCTION
int main()
{
    ArrList<int> list;

    list.push_back(1);
    list.push_back(2);
    list.push_back(3);
    list.push_back(4);
    list.push_back(5);

    // C++11 range-based for loops require an iterator subclass to function
    for (auto i : list)
        std::cout << i << "  ";

    std::cout << "\n\n";

    ArrList<double> other;

    other.push_back(1.1);
    other.push_back(2.2);
    other.push_back(3.3);
    other.push_back(4.4);
    other.push_back(5.5);
    other.push_back(6.6);
    other.push_back(7.7);

    // Demonstrating how an iterator can be explicitly used
    for (ArrList<double>::iterator it = other.begin(); it != other.end(); ++it)
        std::cout << *it << "  ";
    std::cout << '\n';

    return 0;
}


// ArrList Functions
template <typename T>
ArrList<T>::ArrList()
  : _arr(new T[0])
  , _size(0)
  , _capacity(0)
{}

template <typename T>
ArrList<T>::ArrList(const ArrList<T>& cop)
  : _arr(new T[cop._size])
  , _size(cop._size)
  , _capacity(cop._size)
{
    for (size_t i = 0; i < cop._size; i++)
        _arr[i] = cop._arr[i];
}

template <typename T>
ArrList<T>::~ArrList()
{
    delete[] _arr;
    _arr      = nullptr;
    _size     = 0;
    _capacity = 0;
}

template <typename T>
void ArrList<T>::push_back(T item)
{
    if (_size == 0 || _size == _capacity)
        grow();

    _arr[_size] = item;
    ++_size;
}

template <typename T>
size_t ArrList<T>::size() const
{
    return _size;
}

template <typename T>
size_t ArrList<T>::capacity() const
{
    return _capacity;
}

template <typename T>
typename ArrList<T>::iterator ArrList<T>::begin() const
{
    return ArrList<T>::iterator(_arr);
}

template <typename T>
typename ArrList<T>::iterator ArrList<T>::end() const
{
    return ArrList<T>::iterator(_arr + _size);
}

template <typename T>
ArrList<T>& ArrList<T>::operator=(const ArrList<T>& rhs)
{
    if (this != &rhs) {
        delete[] _arr;
        _arr = new T[rhs._size];

        for (size_t i = 0; i < rhs._size; i++)
            _arr[i] = rhs._arr[i];
    }

    return *this;
}

template <typename T>
void ArrList<T>::grow()
{
    if (_capacity == 0) {
        delete[] _arr;
        _arr = new T[1];
        _capacity++;
    } else {
        T* temp = new T[_capacity * 2];
        for (size_t i = 0; i < _size; i++)
            temp[i] = std::move(_arr[i]);

        delete[] _arr;
        _arr = temp;
        _capacity *= 2;
    }
}


// ArrList iterator Functions
template <typename T>
ArrList<T>::iterator::iterator(T* loc)
  : _indexLoc(loc)
{}

template <typename T>
typename ArrList<T>::iterator& ArrList<T>::iterator::operator++()
{
    ++_indexLoc;

    return *this;
}

template <typename T>
const typename ArrList<T>::iterator ArrList<T>::iterator::operator++(int)
{
    ArrList<T>::iterator temp = *this;
    ++(*this);

    return temp;
}

template <typename T>
T& ArrList<T>::iterator::operator*()
{
    return *_indexLoc;
}
