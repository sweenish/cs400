#include <iostream>
#include <memory>
#include <string>

// NOTE: This file is not meant to be compiled!
// We can immediately see that the Book class is looking better, single-purposed
class Book {
public:
    Book(std::string l, std::string f, std::string t, int year);
    Book(const Book& cop);
    Book(Book&& mov);
    Book&       operator=(const Book& rhs);
    Book&       operator=(Book&& rhs);
    friend bool operator==(const Book& lhs, const Book& rhs);
    friend bool operator!=(const Book& lhs, const Book& rhs);
    friend bool operator<(const Book& lhs, const Book& rhs);
    friend bool operator<=(const Book& lhs, const Book& rhs);
    friend bool operator>(const Book& lhs, const Book& rhs);
    friend bool operator>=(const Book& lhs, const Book& rhs);

private:
    std::string _surname;
    std::string _forename;
    std::string _title;
    int         year;
};

// We've separated the "shelf" into a class of its own, another single purpose
// class. This is much better since this "shelf" can hold whatever we need it
// to. One shelf per item type
template <typename T>
class ArrayList {
public:
    ArrayList();
    ArrayList(const ArrayList& cop);
    ArrayList(ArrayList&& mov);
    ArrayList(T& item);
    void insert(const T& item);
    void insert(T&& item);

    class iterator;
    iterator begin();
    iterator end();
    iterator erase(Book book);

    Book&       at(size_t i);
    const Book& at(size_t i) const;
    ArrayList&      operator=(const ArrayList& rhs);
    ArrayList&      operator=(ArrayList&& rhs);

private:
    T* _lib;

    static std::allocator<T> alloc;

    void grow();
    void collapse();
};

/*
 * No class implementations provided!
 *
 * By pulling the "shelf" code out of the Book class, we were able to
 * accomplish a few different things. The first thing was that we were able to
 * clean up the Book class. Before, it was a class trying to do too much stuff.
 * It wanted to contain information about a Book, allow access to that
 * information, store books in an array, and provide all the necessary
 * functions to work with that array.
 *
 * The act of being a book is different from storing books.
 *
 * Pulling the "shelf" code out of the book class also allows us to make the
 * class more generic. Now, it can hold more than just books if we need it to.
 * Pulling the shelf out into its own class also allows us to more easily
 * provide some extra functionality like iterators.
 *
 * The next file discuss iterators in a bit more detail.
 */