#include <iostream>

// Code taken from
// https://stackoverflow.com/questions/10460662/recursive-application-of-operator

struct size {
    int width;
    int height;
    size()
      : width(640)
      , height(480)
    {}
};

struct metrics {
    size        s;
    size const* operator->() const
    {
        std::cout << "metrics arrow\n";
        return &s;
    }
};

struct screen {
    metrics m;
    metrics operator->() const
    {
        std::cout << "screen arrow\n";
        return m;
    }
};

int main()
{
    screen s;
    std::cout << s->width << "\n";
}

/*
 * This file demonstrates how the arrow operator can drill down until it finds a
 * pointer. Iterators purposefully end the chain by returning a pointer. The
 * actual drill down mechanism is a niche case, and doesn't have to be taken
 * into consideration unless you want to write a smart-pointer type class.
 */