#include <iostream>
#include <memory>
#include <string>

// NOTE: This file is not meant to be compiled!
class Book {
public:
    Book(std::string l, std::string f, std::string t, int year);
    Book(const Book& cop);
    Book(Book&& mov);
    static void insert(const Book& book);  // Extra to accommodate "Bookshelf"
    static void insert(Book&& book);       // Extra to accommodate "Bookshelf"

    class iterator;             // Extra to accommodate "Bookshelf"
    iterator begin();           // Extra to accommodate "Bookshelf"
    iterator end();             // Extra to accommodate "Bookshelf"
    iterator erase(Book book);  // Extra to accommodate "Bookshelf"

    Book&       at(size_t i);        // Extra to accommodate "Bookshelf"
    const Book& at(size_t i) const;  // Extra to accommodate "Bookshelf"
    Book&       operator=(const Book& rhs);
    Book&       operator=(Book&& rhs);
    friend bool operator==(const Book& lhs, const Book& rhs);
    friend bool operator!=(const Book& lhs, const Book& rhs);
    friend bool operator<(const Book& lhs, const Book& rhs);
    friend bool operator<=(const Book& lhs, const Book& rhs);
    friend bool operator>(const Book& lhs, const Book& rhs);
    friend bool operator>=(const Book& lhs, const Book& rhs);

private:
    std::string _surname;
    std::string _forename;
    std::string _title;
    int         year;

    static std::allocator<Book> alloc;  // Extra to accommodate "Bookshelf"

    static Book* _lib;  // Extra to accommodate "Bookshelf"

    void grow();      // Extra to accommodate "Bookshelf"
    void collapse();  // Extra to accommodate "Bookshelf"
};

// Book::iterator class declaration would go here


/*
 * This code is NOT meant to be run!
 *
 * It is simply a class declaration used to showcase how silly an idea it is to
 * not separate how we contain class objects from the class itself.
 *
 * This Book class is perfectly capable of functioning. The "Bookshelf" is a
 * statically declared dynamic array, so there will only ever be one. Books can
 * add themselves to the Bookshelf when they constructed.
 *
 * Erasing a Book might get interesting. Any general access to the Bookshelf
 * has to go through a static function. Except the iterators, because we might
 * have a need for multiple iterators to exist at once, so they can't be
 * static. So, in that case, I have to access a Book on the shelf to get and
 * interact with an iterator. But I can also do this through whichever Book I
 * want.
 *
 * It's just weird code. Weird code that we can force to work, but why though?
 *
 * Instead of building a bookshelf to hold books (or vinyls, cd's, photos,
 * action figures, Funko Pop figurines(TM), etc.) We are essentially stapling
 * just Books to a piece of wood and to each other. That would be a mess in the
 * real world, and it's a mess in our code.
 *
 * The better solution is to build a shelf on which I can easily place whatever
 * I want.
 */