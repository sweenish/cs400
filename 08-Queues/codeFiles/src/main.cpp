#include "ArrList.hpp"
#include "Queue.hpp"
#include <iostream>
#include <stack>

// Containers must have a push() function
template <typename T, typename... Containers>
void push(T value, Containers&&... containers)
{
    (..., containers.push(value));
}

int main()
{
    Queue<int> q;

    std::stack<int, ArrList<int>> s;

    push(42, q, s);
    push(37, q, s);
    push(55, q, s);
    push(98, q, s);
    push(12, q, s);
    push(29, q, s);

    std::cout << "Queue\n";
    while (!q.empty()) {
        std::cout << q.front() << " ";
        q.pop();
    }
    std::cout << "\n\n";

    std::cout << "Stack\n";
    while (!s.empty()) {
        std::cout << s.top() << " ";
        s.pop();
    }
    std::cout << '\n';

    return 0;
}