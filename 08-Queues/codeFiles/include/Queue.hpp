#ifndef QQ_HPP
#define QQ_HPP

#include "ArrList.hpp"

template <typename T>
class Queue {
public:
    Queue();
    ~Queue();
    bool   empty() const noexcept;
    size_t size() const noexcept;
    T&     front();
    T&     back();
    void   push(T item) noexcept;
    void   pop();

private:
    ArrList<T>* _list;
};

template <typename T>
Queue<T>::Queue()
  : _list(new ArrList<T>())
{}

template <typename T>
Queue<T>::~Queue()
{
    delete _list;
}

template <typename T>
bool Queue<T>::empty() const noexcept
{
    return _list->empty();
}

template <typename T>
size_t Queue<T>::size() const noexcept
{
    return _list->size();
}

template <typename T>
T& Queue<T>::front()
{
    return _list->front();
}

template <typename T>
T& Queue<T>::back()
{
    return _list->back();
}

template <typename T>
void Queue<T>::push(T item) noexcept
{
    _list->push_back(item);
}

template <typename T>
void Queue<T>::pop()
{
    _list->pop_front();
}

#endif