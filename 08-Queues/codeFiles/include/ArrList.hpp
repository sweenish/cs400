/*
 * NAME:        Adam Sweeney
 * WSUID:       x111x111
 * HW:          1
 * Description: A template Arrlist to hold pointers to things
 */

#ifndef ARRAY_ArrLIST_HPP
#define ARRAY_ArrLIST_HPP

#include <memory>
#include <stdexcept>
#include <type_traits>
#include <typeinfo>

template <typename T>
class ArrList {
public:
    ArrList();
    ArrList(T item);
    ArrList(const ArrList<T>& rhs);
    ArrList(ArrList<T>&& rhs);
    ~ArrList();
    ArrList<T>& operator=(const ArrList<T>& rhs);
    ArrList<T>& operator=(ArrList<T>&& rhs);
    size_t      size() const noexcept;
    size_t      capacity() const noexcept;
    void        insert(T item) noexcept;
    void        push_back(T item) noexcept;  // EXTRA for std::stack
    void        pop_front();                 // EXTRA for queue
    void        pop_back();                  // EXTRA for std::stack
    T&          front();                     // EXTRA for queue
    T&          back();                      // EXTRA for std::stack
    bool        empty() const noexcept;      // EXTRA for std::stack
    void     erase(T item);  // Throws underflow_error if empty, range_error if item doesn't exist
    const T& at(size_t i) const;    // Both at() functions can throw
    T&       at(size_t i);          // out_of_range exception
    size_t   search(T item) const;  // Throws domain_error if item doesn't exist

    using value_type      = T;  // Aliases for std::stack
    using reference       = value_type&;
    using const_reference = const value_type&;
    using size_type       = size_t;

private:
    T* begin_m;
    T* end_m;
    T* capacity_m;

    static std::allocator<T> alloc;
    using allocTraits = std::allocator_traits<decltype(alloc)>;

    void grow() noexcept;              // Not required
    void displace(size_t i);           // Not required
    void collapse(size_t i) noexcept;  // Not required
    void scrub() noexcept;             // Not required
};

template <typename T>
std::allocator<T> ArrList<T>::alloc;

// ArrLIST CLASS IMPLEMENTATION
template <typename T>
ArrList<T>::ArrList()
  : begin_m(allocTraits::allocate(alloc, 0))
  , end_m(begin_m)
  , capacity_m(begin_m)
{}

template <typename T>
ArrList<T>::ArrList(T item)
  : begin_m(allocTraits::allocate(alloc, 1))
  , end_m(begin_m)
  , capacity_m(begin_m + 1)
{
    allocTraits::construct(alloc, end_m++, item);
}

template <typename T>
ArrList<T>::ArrList(const ArrList<T>& rhs)
  : begin_m(allocTraits::allocate(alloc, rhs.size()))
  , end_m(begin_m)
  , capacity_m(begin_m + rhs.size())
{
    for (size_t i = 0; i < rhs.size(); i++) {
        allocTraits::construct(alloc, end_m++, rhs.begin_m[i]);
    }
}

template <typename T>
ArrList<T>::ArrList(ArrList<T>&& rhs)
  : begin_m(rhs.begin_m)
  , end_m(rhs.end_m)
  , capacity_m(rhs.capacity_m)
{
    rhs.begin_m    = nullptr;
    rhs.end_m      = nullptr;
    rhs.capacity_m = nullptr;
}

template <typename T>
ArrList<T>::~ArrList()
{
    scrub();
    begin_m = end_m = capacity_m = nullptr;
}

template <typename T>
ArrList<T>& ArrList<T>::operator=(const ArrList<T>& rhs)
{
    if (this != &rhs) {
        scrub();
        begin_m    = allocTraits::allocate(alloc, rhs.size());
        end_m      = begin_m;
        capacity_m = begin_m + rhs.size();
        for (size_t i = 0; i < rhs.size(); i++)
            allocTraits::construct(end_m++, rhs.begin_m[i]);
    }

    return *this;
}

template <typename T>
ArrList<T>& ArrList<T>::operator=(ArrList<T>&& rhs)
{
    if (this != &rhs) {
        scrub();
        begin_m        = rhs.begin_m;
        end_m          = rhs.end_m;
        capacity_m     = rhs.capacity_m;
        rhs.begin_m    = nullptr;
        rhs.end_m      = nullptr;
        rhs.capacity_m = nullptr;
    }

    return *this;
}

template <typename T>
size_t ArrList<T>::size() const noexcept
{
    return end_m - begin_m;
}

template <typename T>
size_t ArrList<T>::capacity() const noexcept
{
    return capacity_m - begin_m;
}

template <typename T>
void ArrList<T>::insert(T item) noexcept
{
    if (capacity() == 0 || size() == capacity()) {
        grow();
    }

    if (size() == 0) {
        allocTraits::construct(alloc, end_m++, item);
        return;
    }

    size_t i = 0;
    while (i < size()) {
        if (*item < *(begin_m[i])) {
            displace(i);
            break;
        }
        i++;
    }

    allocTraits::construct(alloc, begin_m + i, item);
    if (i == size()) {
        ++end_m;
    }
}

template <typename T>
void ArrList<T>::push_back(T item) noexcept
{
    if (capacity() == 0 || size() == capacity()) {
        grow();
    }

    if (size() == 0) {
        allocTraits::construct(alloc, end_m++, item);
        return;
    }

    allocTraits::construct(alloc, end_m++, item);
}

template <typename T>
void ArrList<T>::pop_front()
{
    if (size() == 0) {
        throw std::underflow_error("Pop on Empty\n");
    }

    T* tmp = begin_m;
    while (tmp + 1 != end_m) {
        std::swap(*tmp, *(tmp + 1));
        ++tmp;
    }

    pop_back();
}

template <typename T>
void ArrList<T>::pop_back()
{
    if (size() == 0) {
        throw std::underflow_error("Pop on Empty\n");
    }

    allocTraits::destroy(alloc, --end_m);
}

template <typename T>
T& ArrList<T>::front()
{
    if (size() == 0) {
        throw std::underflow_error("Front() on Empty\n");
    }

    return *begin_m;
}

template <typename T>
T& ArrList<T>::back()
{
    if (size() == 0) {
        throw std::underflow_error("Back() on Empty\n");
    }

    T* last = end_m - 1;

    return *last;
}

template <typename T>
bool ArrList<T>::empty() const noexcept
{
    return begin_m == end_m;
}

template <typename T>
void ArrList<T>::erase(T item)
{
    if (size() == 0) {
        throw std::underflow_error("erase on empty");
    }

    try {
        size_t loc = search(item);

        collapse(loc);
    } catch (std::domain_error& e) {
        throw std::range_error("erase non-existent item");
    }
}

template <typename T>
const T& ArrList<T>::at(size_t i) const
{
    if (i < 0 || i >= size()) {
        throw std::out_of_range("bad index");
    }

    return begin_m[i];
}

template <typename T>
T& ArrList<T>::at(size_t i)
{
    if (i < 0 || i >= size()) {
        throw std::out_of_range("bad index");
    }

    return begin_m[i];
}

template <typename T>
size_t ArrList<T>::search(T item) const
{
    for (unsigned int i = 0; i < size(); i++) {
        if (begin_m[i] == item) {
            return i;
        }
    }

    throw std::domain_error("does not exist");
}

template <typename T>
void ArrList<T>::grow() noexcept
{
    if (capacity_m == begin_m) {
        allocTraits::deallocate(alloc, begin_m, size());
        begin_m = allocTraits::allocate(alloc, 1);
        ++capacity_m;
    } else {
        T* temp    = allocTraits::allocate(alloc, size() * 2);
        T* tempEnd = temp;
        for (size_t i = 0; i < size(); i++) {
            allocTraits::construct(alloc, tempEnd++, std::move(*(begin_m + i)));
        }
        scrub();
        begin_m    = temp;
        end_m      = tempEnd;
        capacity_m = begin_m + size() * 2;
    }
}

template <typename T>
void ArrList<T>::displace(size_t i)
{
    if (!(size() < capacity())) {
        throw std::range_error("displace has no room");
    }

    allocTraits::construct(alloc, end_m++, std::move(*(begin_m + size() - 1)));
    for (T* counter = end_m - 2; counter != begin_m + i; counter--) {
        *counter = std::move(*(counter - 1));
    }

    allocTraits::destroy(alloc, begin_m + i);
}

template <typename T>
void ArrList<T>::collapse(size_t i) noexcept
{
    for (unsigned int counter = i; counter < size() - 1; counter++) {
        allocTraits::construct(alloc, begin_m + counter, std::move(*(begin_m + counter + 1)));
    }
    allocTraits::destroy(alloc, --end_m);
}

template <typename T>
void ArrList<T>::scrub() noexcept
{
    if (begin_m != nullptr) {
        while (end_m != begin_m) {
            allocTraits::destroy(alloc, --end_m);
        }
    }

    allocTraits::deallocate(alloc, begin_m, capacity());
}

#endif